<?php

namespace App\Form;

use App\Entity\IeSemester;
use App\Entity\IeStudentInformation;
use App\Entity\IeVague;
use App\Repository\IeSemesterRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ValidationStudentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('faculty', null, [
                'attr' => [
                    'class' => 'form-control si_faculty text-uppercase',
                ],
                'label' => 'Filière :'
            ])
            ->add('level', null, [
                'attr' => [
                    'class' => 'form-control si_level text-uppercase',
                ],
                'label' => 'Niveau :'
            ]);

        if ($options['level']) {
            global $level;
            $level =  $options['level'];
            $builder->add('semester', EntityType::class, [
                'label' => 'Semestre : ',
                'class' => IeSemester::class,
                // 'choice_label' => 'number',
                'query_builder' => function (IeSemesterRepository $ieSemesterRepository) {
                    global $level;
                    return $ieSemesterRepository->getSemesterByLevel($level);
                },
                'attr' => [
                    'class' => 'form-control si_semester'
                ]
            ]);
        } else {
            $builder->add('semester', null, [
                'label' => 'Semestre : ',
                'attr' => [
                    'class' => 'form-control si_semester'
                ]
            ]);
        }

        $builder
            ->add('vague', EntityType::class, [
                'attr' => [
                    'class' => 'form-control si_vague'
                ],
                'label' => 'Vague',
                'class' => IeVague::class,
            ]);

        if ($options['idNumber']) {
            $builder->add('numero', TextType::class, [
                'attr' => [
                    'class' => 'form-control sinumero_',
                    'value' => $options['idNumber']
                    // 'placeholder' => '- entrez un numero de l\'étudiant -',
                ],
                'mapped' => false,
                'required' => true,
                'label' => 'Numero : ',
            ]);
        } else {
            $builder->add('numero', TextType::class, [
                'attr' => [
                    'class' => 'form-control sinumero_',
                    'placeholder' => '- entrez un numero de l\'étudiant -',
                ],
                'mapped' => false,
                'required' => true,
                'label' => 'Numero : ',
            ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => IeStudentInformation::class,
            'level' => false,
            'idNumber' => false,
        ]);
    }
}
