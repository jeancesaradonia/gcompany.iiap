<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class PayementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('doneAt', DateType::class, [
                'label' => 'Date',
                'attr' => [
                    'class' => "form-control",
                ],
                'widget' => "single_text"
            ])
            ->add('referenceMobileMoney', TextType::class, [
                'label' => 'Numero de référence',
                'attr' => [
                    'class' => "form-control",
                    'placeholder' => 'Exemple : 1417214491'
                ]
            ]);

        if ($options['motif'] && $options['motif'] === "monthly-fees") {
            $builder
                ->add('numberMonth', NumberType::class, [
                    'label' => 'Nombre de mois à payer',
                    'html5' => true,
                    'attr' => [
                        'class' => "form-control pt_numberMonth",
                    ],
                ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'motif' => false,
        ]);
    }
}
