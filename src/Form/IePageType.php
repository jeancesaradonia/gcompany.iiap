<?php

namespace App\Form;

use App\Entity\IePage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IePageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', null, [
                'label' => 'Titre de la page'
            ])
            ->add('slug', null, [
                'label' => 'URL dans le navigateur'
            ])
            ->add('description')
            ->add('linkText', null, [
                'label' => 'Nom du lien'
            ])
            ->add('menuOrder', null, [
                'label' => 'Ordre dans le menu'
            ])
            ->add('view', null, [
                'label' => 'Vue à rendre à l\'utilisateur'
            ])
            ->add('isActive', null, [
                'label' => 'Active?'
            ])
            ->add('pages', null, [
                'label' => 'Ajouter un sous menu ici -',
                'attr' => [
                    'class' => 'form-control sous-menu-select',
                    'data-placeholder' => '- Séléctionner une ou plusieurs sous-menu ici -'

                ],
                'multiple'      => true,
                'expanded'      => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => IePage::class,
        ]);
    }
}
