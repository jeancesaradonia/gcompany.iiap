<?php

namespace App\Form;

use App\Entity\IeLevel;
use App\Entity\IeFaculty;
use App\Repository\IeLevelRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class RegistrationStudentFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder

            // COMMON USER
            ->add('lastName', TextType::class, [
                'label' => 'Nom',
                'attr' => [
                    'placeholder' => 'Votre nom',
                    'class' => 'form-control rsf_lastName'
                ]
            ])
            ->add('firstName', TextType::class, [
                'label' => 'Prénom',
                'attr' => [
                    'placeholder' => 'Votre prénom',
                    'class' => 'form-control rsf_firstName'
                ]
            ])
            ->add('email', EmailType::class, [
                'label' => 'Email',
                'required'  => true,
                'attr' => [
                    'placeholder' => 'Votre e-mail',
                    'class' => 'form-control rsf_email'
                ]
            ])
            ->add('phoneNumber', TelType::class, [
                'label' => 'Contact',
                'attr' => [
                    'placeholder' => 'Votre numero de téléphone',
                    'class' => 'form-control rsf_phoneNumber'
                ]
            ])
            ->add('birthDate', DateTimeType::class, [
                'label' => 'Date de naissance',
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'form-control rsf_birthDate'
                ]
            ])
            ->add('birthPlace', TextType::class, [
                'label' => 'Lieu de naissance',
                'attr' => [
                    'placeholder' => 'Votre lieu de naissance',
                    'class' => 'form-control rsf_birthPlace'
                ]
            ])
            ->add('address', TextType::class, [
                'label' => 'Addresse',
                'attr' => [
                    'placeholder' => 'Votre addresse actuel',
                    'class' => 'form-control rsf_address'
                ]
            ])
            ->add('imageFile', VichImageType::class, [
                'label' => 'Photo d\'identité',
                'required'  => true,
                'allow_delete'  => true,
                'download_label'  => 'Télécharger',
                'download_uri'  => true,
                'image_uri'  => true,
                'imagine_pattern'  => 'thumbnail_list',
                'asset_helper'  => true,
                'attr' => [
                    'class' => 'form-control imageFileClass rsf_imageFile'
                ]
            ])
            ->add('agreeTerms', CheckboxType::class, [
                'mapped' => false,
                'label' => "Accepter les conditions",
                'attr' => [
                    'class' => 'rsf_agreeTerms'
                ],
                'constraints' => [
                    new IsTrue([
                        'message' => 'Vous devez accepter nos conditions.',
                    ]),
                ],
            ])




            // STUDENT INFORMATION

            // ->add('matriculeNumber')
            ->add('employerName', TextType::class, [
                'label' => 'Nom de l\'employeur',
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'Nom de l\'employeur',
                    'class' => 'form-control rsf_employerName'
                ]
            ])
            ->add('job', TextType::class, [
                'label' => 'Fonction',
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'Votre fonction',
                    'class' => 'form-control rsf_job'
                ]
            ])
            ->add(
                'fmNames',
                TextType::class,
                [
                    'label' => 'Nom et prénom(s) du père ou de la mère',
                    'mapped' => false,
                    'attr' => [
                        'placeholder' => 'Nom et prénom du père ou de la mère',
                        'class' => 'form-control rsf_fmNames'
                    ]
                ]
            )
            ->add('fmJobs', TextType::class, [
                'label' => 'Fonction',
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'Fonction',
                    'class' => 'form-control rsf_fmJobs'
                ]
            ])
            ->add('fmEmail', EmailType::class, [
                'label' => 'E-mail',
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'E-mail',
                    'class' => 'form-control rsf_fmEmail'
                ]
            ])
            ->add('fmPhoneNumber', TelType::class, [
                'label' => 'Contact',
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'Contact',
                    'class' => 'form-control rsf_fmPhoneNumber'
                ]
            ])
            ->add('fmAdress', TextType::class, [
                'label' => 'Adresse',
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'Adresse',
                    'class' => 'form-control rsf_fmAdress'
                ]
            ])
            ->add('tNames', TextType::class, [
                'label' => 'Nom et prénom(s) du tuteur ou de la tutrice',
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'nom et prénom(s) du tuteur ou de la tutrice',
                    'class' => 'form-control rsf_tNames'
                ]
            ])
            ->add('tJobs', TextType::class, [
                'label' => 'Fonction',
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'fonction',
                    'class' => 'form-control rsf_tJobs'
                ]
            ])
            ->add('tEmail', EmailType::class, [
                'label' => 'E-mail',
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'e-mail',
                    'class' => 'form-control rsf_tEmail'
                ]
            ])
            ->add('tPhoneNumber', TelType::class, [
                'label' => 'Contact',
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'contact',
                    'class' => 'form-control rsf_tPhoneNumber'
                ]
            ])
            ->add('tAdress', TextType::class, [
                'label' => 'Adresse',
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'adresse',
                    'class' => 'form-control rsf_tAdress'
                ]
            ])
            ->add('type', ChoiceType::class, [
                'label' => 'Êtes-vous?',
                'mapped' => false,
                'choices' => [
                    "Bachelier/ère" => "bachelor",
                    "travailleur(euse)" => "worker"
                ],
                'attr' => [
                    'class' => 'form-control rsf_type'
                ]
            ])
            ->add('gNames', TextType::class, [
                'label' => 'Nom du garant',
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'nom du garant',
                    'class' => 'form-control rsf_gNames'
                ]
            ])
            ->add('gEmail', EmailType::class, [
                'label' => 'E-mail',
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'e-mail',
                    'class' => 'form-control rsf_gEmail'
                ]
            ])
            ->add('gPhoneNumber', TelType::class, [
                'label' => 'Contact',
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'contact',
                    'class' => 'form-control rsf_gPhoneNumber'
                ]
            ])
            ->add('level', EntityType::class, [
                'label' => 'Niveau',
                'mapped' => false,
                'class' => IeLevel::class,
                'choice_label' => 'name',
                'query_builder' => function (IeLevelRepository $ieLevelRepository) {
                    return $ieLevelRepository->getLevelForRegistrationFront();
                },
                'attr' => [
                    'class' => 'form-control rsf_level'
                ]
            ])
            ->add('faculty', EntityType::class, [
                'label' => 'Filière',
                'mapped' => false,
                'class' => IeFaculty::class,
                'choice_label' => 'name',
                'attr' => [
                    'class' => 'form-control rsf_level'
                ]
            ])

            // NEW

            ->add('formationType', ChoiceType::class, [
                'label' => 'Modalité d\'inscription ?',
                'mapped' => false,
                'choices' => [
                    "Formation en ligne" => "online",
                    "Formation en présentiel" => "face-to-face"
                ],
                'attr' => [
                    'class' => 'form-control rsf_formationType'
                ]
            ])


            ->add('sexe', ChoiceType::class, [
                'label' => 'Sexe',
                'mapped' => false,
                'choices' => [
                    "Masculin" => "male",
                    "Feminin" => "feminine"
                ],
                'attr' => [
                    'class' => 'form-control rsf_sexe'
                ]
            ])

            ->add('nativeCountry', CountryType::class, [
                'label' => 'Pays d\'origine',
                'mapped' => false,
            ])

            ->add('cinPhoto', FileType::class, [
                'label' => 'CIN *',
                'mapped' => false,
                'required' => true,
                'attr' => [
                    'class' => 'form-control rsf_cinPhoto'
                ],
                'constraints' => [
                    new File([
                        'maxSize' => '10240k',
                        'maxSizeMessage' => 'Veuillez télécharger un document PDF valide',
                    ]),
                    new NotBlank([
                        'message'  => 'Il faut le remplir'
                    ])
                ],
            ])

            ->add('curriculumVitae', FileType::class, [
                'label' => 'Curriculum vitae *',
                'mapped' => false,
                'required' => true,
                'attr' => [
                    'class' => 'form-control rsf_curriculumVitae'
                ],
                'constraints' => [
                    new File([
                        'maxSize' => '10240k',
                        'maxSizeMessage' => 'Veuillez télécharger un document PDF valide',
                    ]),
                    new NotBlank([
                        'message'  => 'Il faut le remplir'
                    ])

                ],
            ])

            ->add('noteOrCopyOfTheCertifiedDiploma', FileType::class, [
                'label' => 'Note ou copie du diplome certifié *',
                'mapped' => false,
                'required' => true,
                'attr' => [
                    'class' => 'form-control rsf_noteOrCopyOfTheCertifiedDiploma'
                ],
                'constraints' => [
                    new File([
                        'maxSize' => '10240k',
                        'maxSizeMessage' => 'Veuillez télécharger un document PDF valide',
                    ]),
                    new NotBlank([
                        'message'  => 'Il faut le remplir'
                    ])
                ],
            ])

            ->add('residenceCertificate', FileType::class, [
                'label' => 'Certificat de residance *',
                'mapped' => false,
                'required' => true,
                'attr' => [
                    'class' => 'form-control rsf_residenceCertificate'
                ],
                'constraints' => [
                    new File([
                        'maxSize' => '10240k',
                        'maxSizeMessage' => 'Veuillez télécharger un document PDF valide',
                    ]),
                    new NotBlank([
                        'message'  => 'Il faut le remplir'
                    ])
                ],
            ])

            ->add('birthCertificate', FileType::class, [
                'label' => 'Buillet de naissace *',
                'mapped' => false,
                'required' => true,
                'attr' => [
                    'class' => 'form-control rsf_birthCertificate'
                ],
                'constraints' => [
                    new File([
                        'maxSize' => '10240k',
                        'maxSizeMessage' => 'Veuillez télécharger un document PDF valide',
                    ]),
                    new NotBlank([
                        'message'  => 'Il faut le remplir'
                    ])
                ],
            ])


            ->add('motivationLetter', FileType::class, [
                'label' => 'Lettre de motivation *',
                'mapped' => false,
                'required' => true,
                'attr' => [
                    'class' => 'form-control rsf_motivationLetter'
                ],
                'constraints' => [
                    new File([
                        'maxSize' => '10240k',
                        'maxSizeMessage' => 'Veuillez télécharger un document PDF valide',
                    ]),
                    new NotBlank([
                        'message'  => 'Il faut le remplir'
                    ])
                ],
            ])




            //
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
