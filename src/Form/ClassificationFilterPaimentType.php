<?php

namespace App\Form;

use App\Entity\IePayment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ClassificationFilterPaimentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $builder
            ->add('reason', ChoiceType::class, [
                'label' => 'Motif du paiment?',
                'placeholder' => '-----',
                'required' => false,
                'choices' => [
                    "Frais d'inscription" => "registration-fee",
                    "Écolage" => "monthly-fees",
                    "Droit d'examen" => "examination-fees",
                    "Droit de soutenance" => "defense-fee",
                    "Certificat" => "certificate-fee",
                ],
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('state', ChoiceType::class, [
                'placeholder' => '-----',
                'required' => false,
                'label' => 'État du paiment?',
                'choices' => [
                    "Lu" => "read",
                    "Non lu" => "unread",
                ],
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('decision', ChoiceType::class, [
                'label' => 'Décision sur le paiment?',
                'placeholder' => '-----',
                'required' => false,
                'choices' => [
                    "Prise" => "taken",
                    "Non Prise" => "not taken",
                ],
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('validatedAt', null, [
                'label' => 'Date de validation',
                'required' => false,
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('type', ChoiceType::class, [
                'placeholder' => '-----',
                'required' => false,
                'label' => 'mode de paiment?',
                'choices' => [
                    "Mobile Money" => "mobile-money",
                ],
                'attr' => [
                    'class' => 'form-control'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => IePayment::class,
        ]);
    }
}
