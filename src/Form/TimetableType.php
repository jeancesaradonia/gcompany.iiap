<?php

namespace App\Form;

use App\Entity\IeTimetable;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class TimetableType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('startAt', DateTimeType::class, [
                'label' => 'Débuter à',
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'form-control tt_startAt'
                ]
            ])
            ->add('endAt', DateTimeType::class, [
                'label' => 'Términer à',
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'form-control tt_endAt'
                ]
            ])
            ->add('title', null, [
                'label' => 'Titre',
                'attr' => [
                    'class' => 'form-control tt_title'
                ]
            ])
            ->add('description', null, [
                'label' => 'Description',
                'attr' => [
                    'class' => 'form-control tt_description'
                ]
            ])
            ->add('color', ColorType::class, [
                'label' => 'Couleur d\'arrière plan pour le titre',
                'attr' => [
                    'class' => 'form-control tt_target'
                ]
            ])
            ->add('target', ChoiceType::class, [
                'label' => 'Cible ?',
                'choices' => [
                    "Formation en ligne" => "online",
                    "Formation en présentiel" => "face-to-face"
                ],
                'attr' => [
                    'class' => 'form-control tt_target'
                ]
            ])
            ->add('faculty', null, [
                'label' => "Filière",
                'placeholder' => "- filière -",
                'attr' => [
                    'class' => 'form-control tt_faculty'
                ]
            ])
            ->add('level', null, [
                'label' => "Niveau",
                'placeholder' => "- niveau -",
                'attr' => [
                    'class' => 'form-control tt_level'
                ]
            ])
            ->add('vague', null, [
                'label' => "Vague",
                'placeholder' => "- vague -",
                'attr' => [
                    'class' => 'form-control tt_vague'
                ]
            ])
            ->add('semester', null, [
                'label' => "Semester",
                'placeholder' => "- semester -",
                'attr' => [
                    'class' => 'form-control tt_semester'
                ]
            ]);;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => IeTimetable::class,
        ]);
    }
}
