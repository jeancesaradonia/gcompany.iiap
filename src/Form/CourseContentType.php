<?php

namespace App\Form;

use App\Entity\IeCourseContent;
use Symfony\Component\Form\AbstractType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class CourseContentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('contentType', ChoiceType::class, [
                'label' => 'Type de contenu',
                'required'  => true,
                'choices' => [
                    'MIXE' => "MIXE",
                    'PDF' => "PDF",
                    'AUDIO' => "AUDIO",
                    'VIDEO' => "VIDEO",
                    'TEXTE' => "TEXTE",
                ],
                'attr' => [
                    'class' => 'form-control multiple-select'
                ]
            ])
            ->add('content', CKEditorType::class, [
                'config' => array('toolbar' => 'full'),
                'label' => 'Contenu de',
                'required'  => true,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('courseType', ChoiceType::class, [
                'label' => 'Type de course',
                'required'  => true,
                'choices' => [
                    'Leçon' => "LESSON",
                    'Exercice' => "EXERCICE",
                    'Correction' => "CORRECTION",
                ],
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('imageFile', VichImageType::class, [
                'label' => 'Le fichier à importer !',
                'required'  => false,
                'allow_delete'  => true,
                'download_label'  => 'Télécharger',
                'download_uri'  => true,
                'image_uri'  => true,
                'imagine_pattern'  => 'thumbnail_list',
                'asset_helper'  => true,
                'attr' => [
                    'class' => 'form-control imageFileClass'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => IeCourseContent::class,
        ]);
    }
}
