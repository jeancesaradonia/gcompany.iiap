<?php

namespace App\Form;

use App\Entity\IeVague;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VagueType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('vNumber', null, [
                'label' => 'Numero *',
                'required' => true
            ])
            ->add('vDescription', null, [
                'label' => 'Description',
                'required' => false
            ])
            ->add('startAt', null, [
                'label' => 'Date d\'entrée',
                'required' => false,
                'widget' => 'single_text',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => IeVague::class,
        ]);
    }
}
