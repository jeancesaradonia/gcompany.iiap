<?php

namespace App\Form;

use App\Entity\IeStudentInformation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class FilterStudentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('type', ChoiceType::class, [
                'placeholder' => "- Type De Formation -",
                'label' => 'Modalité d\'inscription ?',
                'required' => false,
                'choices' => [
                    "Formation en ligne" => "online",
                    "Formation en présentiel" => "face-to-face"
                ],
                'attr' => [
                    'class' => 'form-control rsf_formationType'
                ]
            ])
            ->add('level', null, [
                'placeholder' => "- Niveau Des Étudiants -",
                'required' => false,
                'label' => 'Niveau ?',
                'attr' => [
                    'class' => 'form-control fs_level'
                ]
            ])
            ->add('semester', null, [
                'placeholder' => "- Dans Quel Semestre -",
                'required' => false,
                'label' => 'Semestre ?',
                'attr' => [
                    'class' => 'form-control fs_semester'
                ]
            ])
            ->add('vague', null, [
                'required' => false,
                'placeholder' => "- Quelle Vague -",
                'label' => 'Vague ?',
                'attr' => [
                    'class' => 'form-control fs_vague'
                ]
            ]);

        if ($options['faculty'] === "allow") {
            $builder
                ->add('faculty', null, [
                    'placeholder' => '- Filière -',
                    'required' => false,
                ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => IeStudentInformation::class,
            'faculty' => false
        ]);
    }
}
