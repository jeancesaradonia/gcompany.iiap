<?php

namespace App\Form;

use App\Entity\IeCourse;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CourseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', null, [
                'label' => 'Titre (le nom) du matière',
                'required'  => true,
                'attr' => [
                    'placeholder' => 'Algorithme',
                    'class' => 'form-control'
                ]
            ])
            ->add('description', null, [
                'label' => 'Description de la matière',
                'required'  => false,
                'attr' => [
                    'placeholder' => 'behind the word mountains, far from the countries Vokalia an',
                    'class' => 'form-control'
                ]
            ])
            ->add('credit', null, [
                'label' => 'Crédit',
                'required'  => false,
                'attr' => [
                    'placeholder' => '3',
                    'class' => 'form-control'
                ]
            ])
            ->add('teachingUnit', null, [
                'label' => 'Unité d\'enseignement',
            ])
            ->add('imageFile', VichImageType::class, [
                'label' => 'Image de couverture de cette matière',
                'required'  => true,
                'allow_delete'  => true,
                'download_label'  => 'Télécharger',
                'download_uri'  => true,
                'image_uri'  => true,
                'imagine_pattern'  => 'thumbnail_list',
                'asset_helper'  => true,
                'attr' => [
                    'class' => 'form-control imageFileClass'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => IeCourse::class,
        ]);
    }
}
