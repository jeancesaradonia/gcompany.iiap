<?php

namespace App\Form;

use App\Entity\IeTimetable;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class TimetableFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('target', ChoiceType::class, [
                'label' => 'Modalité d\'inscription ?',
                'choices' => [
                    "Formation en ligne" => "online",
                    "Formation en présentiel" => "face-to-face"
                ],
                'attr' => [
                    'class' => 'form-control tft_target'
                ]
            ])
            ->add('faculty', null, [
                'label' => "Filière",
                'placeholder' => "-  -",
                'choice_label' => "acronym",
                'attr' => [
                    'class' => 'form-control tft_faculty'
                ]
            ])
            ->add('level', null, [
                'label' => "Niveau",
                'placeholder' => "-  -",
                'attr' => [
                    'class' => 'form-control tft_level'
                ]
            ])
            ->add('vague', null, [
                'label' => "Vague",
                'placeholder' => "-  -",
                'attr' => [
                    'class' => 'form-control tft_vague'
                ]
            ])
            ->add('semester', null, [
                'label' => "Semester",
                'placeholder' => "-  -",
                'attr' => [
                    'class' => 'form-control tft_semester'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => IeTimetable::class,
        ]);
    }
}
