<?php

namespace App\Form;

use App\Entity\IeTaught;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TaughtType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('faculty', null, [
                'label' => 'Filière',
                'required'  => true,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('semester', null, [
                'label' => 'Semestre',
                'required'  => true,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('teacherInformation', null, [
                'label' => 'Professeur',
                'required'  => true,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('course', null, [
                'label' => 'Matière à ajouter',
                'placeholder' => '- Matière -',
                'required'  => true,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('month', null, [
                'label' => 'Mois',
                'required'  => true,
                'attr' => [
                    'placeholder' => '1',
                    'class' => 'form-control'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => IeTaught::class,
        ]);
    }
}
