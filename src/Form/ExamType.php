<?php

namespace App\Form;

use App\Entity\IeExam;
use Symfony\Component\Form\AbstractType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ExamType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('course', null, [
                'label' => 'Matière',
                'attr' => [
                    'class' => 'formt-control'
                ]
            ])
            ->add('session', null, [
                'placeholder' => "- Session d'examen -",
                'label' => 'Session',
                'attr' => [
                    'class' => 'formt-control'
                ]
            ])
            ->add('type', ChoiceType::class, [
                'label' => 'Type?',
                'choices' => [
                    "Fichier" => "file",
                    "Rédaction" => "question",
                ],
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('duration', null, [
                'label' => 'Durée en minute',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('question', CKEditorType::class, [
                'config' => array('toolbar' => 'full'),
                'label' => 'Question pour la rédaction',
                'required' => false
            ])
            ->add('imageFile', VichImageType::class, [
                'label' => 'Fichier à importer',
                'required'  => false,
                'allow_delete'  => true,
                'download_label'  => 'Télécharger',
                'download_uri'  => true,
                'image_uri'  => true,
                'imagine_pattern'  => 'thumbnail_list',
                'asset_helper'  => true,
                'attr' => [
                    'class' => 'form-control imageFileClass'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => IeExam::class,
        ]);
    }
}
