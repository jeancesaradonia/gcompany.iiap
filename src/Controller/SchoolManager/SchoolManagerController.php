<?php

namespace App\Controller\SchoolManager;

use DateTime;
use App\Entity\IePayment;
use App\Form\ClassificationFilterPaimentType;
use Symfony\Component\Mime\Email;
use App\Repository\IeUserRepository;
use App\Repository\IePaymentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/school-manager", priority=10)
 */
class SchoolManagerController extends AbstractController
{
    /**
     * @Route("/", name="app_school_manager_home")
     */
    public function index(): Response
    {
        return $this->render('school_manager/dashboard.html.twig');
    }

    /**
     * @Route("/payment/request/{type}", name="app_school_manager_payment_request")
     */
    public function paymentRequest(?string $type, Request $request, IePaymentRepository $iePaymentRepository): Response
    {
        $requests = $iePaymentRepository->getRequestUnReadByType($type);
        return $this->render('school_manager/payment/request.html.twig', [
            'requests' => $requests,
            'type' => $type
        ]);
    }


    /**
     * @Route("/payment/validate/{id}", name="app_payment_validate")
     */
    public function paymentValidate(
        IePayment $iePayment,
        Request $request,
        MailerInterface $mailer,
        EntityManagerInterface $entityManagerInterface,
        IeUserRepository $ieUserRepository
    ): Response {
        $iePayment
            ->setState('read')
            ->setDecision('taken')
            ->setObservation($request->request->get('observation_' . $iePayment->getId()))
            ->setValidatedAt(new \DateTime());
        $studentInformation = $iePayment->getStudent();
        switch ($iePayment->getReason()) {
            case 'registration-fee':
                $studentInformation->setRegistrationFee(1);
                break;
            case 'monthly-fees':
                $studentInformation->setMonthlyFees($studentInformation->getMonthlyFees() + ($iePayment->getAmount() / 200000));
                break;
        }
        $student = $ieUserRepository->getUserByStudentInformation($studentInformation);
        $email = (new TemplatedEmail())
            ->from('iiap@gmail.com')
            ->to($student->getEmail())
            ->subject('iiap éducation paiement : ' . $this->getReasonPayment($iePayment->getReason()) . ' par MVOLA')
            ->htmlTemplate("school_manager/payment/email-template.html.twig")
            ->context([
                'reason' =>  $iePayment->getReason(),
                'id' =>  $iePayment->getId(),
                'student' => $student
            ]);
        $mailer->send($email);
        $entityManagerInterface->flush();
        $this->addFlash('success', "Une demande a été validé avec succèss, IDENTIFICATION : " . $iePayment->getId());
        return $this->redirectToRoute('app_school_manager_payment_request', ['type' => $iePayment->getType()]);
    }

    /**
     * @Route("/payment/refuse/{id}", name="app_payment_refuse")
     */
    public function paymentRefuse(
        IePayment $iePayment,
        Request $request,
        MailerInterface $mailer,
        EntityManagerInterface $entityManagerInterface,
        IeUserRepository $ieUserRepository
    ): Response {
        $iePayment
            ->setState('read')
            ->setDecision('not taken')
            ->setObservation($request->request->get('observation_' . $iePayment->getId()));
        $studentInformation = $iePayment->getStudent();
        $student = $ieUserRepository->getUserByStudentInformation($studentInformation);
        $entityManagerInterface->flush();
        $this->addFlash('success', "Une demande a été refusé avec succèss, IDENTIFICATION : " . $iePayment->getId());
        return $this->redirectToRoute('app_school_manager_payment_request', ['type' => $iePayment->getType()]);
    }


    /**
     * @Route("/payment/classification", name="app_payment_classification")
     */
    public function classification(
        Request $request,
        IePaymentRepository $iePaymentRepository
    ): Response {
        $iePayment = new IePayment();
        $form = $this->createForm(ClassificationFilterPaimentType::class, $iePayment);
        $form->handleRequest($request);
        $payments = [];
        if ($form->isSubmitted() && $form->isValid()) {
            $payments = $iePaymentRepository->getClassificationFilter($iePayment);
        }
        return $this->render('school_manager/payment/classification.html.twig', [
            'form' => $form->createView(),
            'payments' => $payments
        ]);
    }



    public function getReasonPayment($reason)
    {
        return [
            'registration-fee' => 'Frais d\'inscription',
            'monthly-fees' => 'Écolage',
        ][$reason];
    }
}
