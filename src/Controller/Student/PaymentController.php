<?php

namespace App\Controller\Student;

use App\Entity\IePayment;
use App\Entity\IeUser;
use App\Form\PayementType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\IeStudentInformationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/student", priority=10)
 */
class PaymentController extends AbstractController
{

    /**
     * @Route("/payment", name="app_payment_show")
     */
    public function show(): Response
    {
        $user = $this->getUser();
        return $this->render('student/payment/show.html.twig', [
            'disableAllMenu' => false,
            'activeMenu' => ' payment',
            'user' => $user,
        ]);
    }

    /**
     * @Route("/payment/registration-fee", name="app_payment_user_registration_fee")
     */
    public function paymentRegistrationFee(Request $request, EntityManagerInterface $entityManagerInterface): Response
    {
        $disableAllMenus = true;
        $mVolaPayment = [
            'doneAt' => new \Datetime('now')
        ];
        $payementForm = $this->createForm(PayementType::class, $mVolaPayment, [
            'motif' => "registration-fee"
        ]);
        $payementForm->handleRequest($request);
        if ($payementForm->isSubmitted() && $payementForm->isValid()) {
            $data = $payementForm->getData();
            $payment = new IePayment();
            $payment->setDoneAt($data['doneAt']);
            $payment->setReferenceMobileMoney($data['referenceMobileMoney']);
            $payment->setReason('registration-fee');
            $payment->setState('unread');
            $payment->setType('mobile-money');
            $payment->setAmount('190000');
            $payment->setStudent($this->getUser()->getStudentInformation());
            $entityManagerInterface->persist($payment);
            $entityManagerInterface->flush();
            $this->addFlash('type', 'Frais d\'inscription');
            return $this->redirectToRoute('app_payment_success');
        }
        return $this->render('student/payment/registration-fee.html.twig', [
            'student' => 'Utilisateur connecté',
            'disableAllMenu' => $disableAllMenus,
            'payementForm' => $payementForm->createView()
        ]);
    }

    /**
     * @Route("/payment/registration-fee/success", name="app_payment_success")
     */
    public function paymentRegistrationFeeSuccess(): Response
    {
        return $this->render('student/payment/success.html.twig', [
            'disableAllMenu' => true,
        ]);
    }


    /**
     * @Route("/payment/monthly-fees", name="app_payment_monthly_fee")
     */
    public function paymentMonthlyFee(Request $request, EntityManagerInterface $entityManagerInterface): Response
    {
        $warnFirstMonthlyFee = false;
        $disableAllMenu = false;
        $studentConnected = $this->getUser();
        $studentInformation = $studentConnected->getStudentInformation();
        $monthlyFees = $studentInformation->getMonthlyFees();
        if ($monthlyFees == 0) {
            $warnFirstMonthlyFee = true;
            $disableAllMenu = true;
        }
        $mVolaPayment = [
            'doneAt' => new \Datetime('now'),
            'numberMonth' => 1
        ];
        $payementForm = $this->createForm(PayementType::class, $mVolaPayment, [
            'motif' => "monthly-fees"
        ]);
        $payementForm->handleRequest($request);
        if ($payementForm->isSubmitted() && $payementForm->isValid()) {
            $data = $payementForm->getData();
            $payment = new IePayment();
            $payment->setDoneAt($data['doneAt']);
            $payment->setReferenceMobileMoney($data['referenceMobileMoney']);
            $payment->setReason('monthly-fees');
            $payment->setState('unread');
            $payment->setType('mobile-money');
            $payment->setAmount((int)$data['numberMonth'] * 200000);
            $payment->setStudent($this->getUser()->getStudentInformation());
            $entityManagerInterface->persist($payment);
            $entityManagerInterface->flush();
            $this->addFlash('type', 'Écolage');
            return $this->redirectToRoute('app_payment_success');
        }
        return $this->render('student/payment/monthly-fees.html.twig', [
            'disableAllMenu' => $disableAllMenu,
            'activeMenu' => " payment",
            'payementForm' => $payementForm->createView(),
            'warnFirstMonthlyFee' => $warnFirstMonthlyFee
        ]);
    }

    /**
     * @Route("/payment/examination-fees", name="app_payment_examination_fee")
     */
    public function paymentExaminationFees(): Response
    {
        dd("examination fees");
    }

    /**
     * @Route("/payment/defense-fee", name="app_payment_defense_fee")
     */
    public function paymentDefenseFee(): Response
    {
        dd("Defense fees");
    }

    /**
     * @Route("/payment/certificate-fee", name="app_payment_Certificate_fee")
     */
    public function paymentCertificateFee(): Response
    {
        dd("Defense fees");
    }
}
