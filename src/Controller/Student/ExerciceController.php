<?php

namespace App\Controller\Student;

use App\Entity\IeCourse;
use App\Repository\IeVagueRepository;
use App\Repository\IeTaughtRepository;
use App\Repository\IeCourseContentRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/student", priority=10)
 */
class ExerciceController extends AbstractController
{

    /**
     * @Route("/exercice", name="app_student_exercice")
     */
    public function index(
        IeVagueRepository $ieVagueRepository,
        IeTaughtRepository $ieTaughtRepository
    ): Response {
        $user = $this->getUser();
        $studentInformation = $this->getUser()->getStudentInformation();
        $studentInformationVauge = $studentInformation->getVague();
        $vagueFound = $ieVagueRepository->findOneByVNumber($studentInformationVauge);
        if ($vagueFound) {
            $dateEnter = $vagueFound->getStartAt();
            $now = new \DateTimeImmutable("now");
            $diff = $dateEnter->diff($now, true);
            $monthDiff = ($diff->m) + 1; // + 1 : pour avoir le différence mois 0
            $formationforStudents = $ieTaughtRepository->getAvalaibleCourse($studentInformation, $monthDiff);
        }
        return $this->render('student/exercice/index.html.twig', [
            'disableAllMenu' => false,
            'activeMenu' => ' exercice',
            'user' => $user,
            'formationforStudents' => $formationforStudents,
        ]);
    }


    /**
     * @Route("/exercice/{slug}", name="app_student_exercice_show")
     */
    public function show(IeCourse $ieCourse, IeCourseContentRepository $ieCourseContentRepository): Response
    {
        $user = $this->getUser();
        $courseContents = $ieCourseContentRepository->getCourse($ieCourse, "EXERCICE");
        return $this->render('student/exercice/show.html.twig', [
            'disableAllMenu' => false,
            'activeMenu' => ' exercice',
            'courseContents' => $courseContents,
            'course' => $ieCourse,
        ]);
    }

    /**
     * @Route("/exercice/{slug}/correction", name="app_student_exercice_show_correction")
     */
    public function correction(IeCourse $ieCourse, IeCourseContentRepository $ieCourseContentRepository): Response
    {
        $user = $this->getUser();
        $courseContents = $ieCourseContentRepository->getCourse($ieCourse, "CORRECTION");
        return $this->render('student/exercice/correction.html.twig', [
            'disableAllMenu' => false,
            'activeMenu' => ' exercice',
            'courseContents' => $courseContents,
            'course' => $ieCourse,
        ]);
    }
}
