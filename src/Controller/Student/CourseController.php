<?php

namespace App\Controller\Student;

use App\Entity\IeCourse;
use App\Entity\IeTaught;
use App\Entity\IeCourseContent;
use App\Repository\IeVagueRepository;
use App\Repository\IeTaughtRepository;
use App\Repository\IeCourseContentRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/student", priority=10)
 */
class CourseController extends AbstractController
{

    /**
     * @Route("/course", name="app_student_course")
     */
    public function index(
        IeVagueRepository $ieVagueRepository,
        IeTaughtRepository $ieTaughtRepository
    ): Response {
        $user = $this->getUser();
        $studentInformation = $this->getUser()->getStudentInformation();
        $studentInformationVauge = $studentInformation->getVague();
        $vagueFound = $ieVagueRepository->findOneByVNumber($studentInformationVauge);
        if ($vagueFound) {
            $dateEnter = $vagueFound->getStartAt();
            $now = new \DateTimeImmutable("now");
            $diff = $dateEnter->diff($now, true);
            $monthDiff = ($diff->m) + 1; // + 1 : pour avoir le différence mois 0
            $formationforStudents = $ieTaughtRepository->getAvalaibleCourse($studentInformation, $monthDiff);
        }
        return $this->render('student/course/index.html.twig', [
            'disableAllMenu' => false,
            'activeMenu' => ' course',
            'user' => $user,
            'formationforStudents' => $formationforStudents,
        ]);
    }


    /**
     * @Route("/course/{slug}", name="app_student_course_show")
     */
    public function show(IeCourse $ieCourse, IeCourseContentRepository $ieCourseContentRepository): Response
    {
        $user = $this->getUser();
        $courseContents = $ieCourseContentRepository->getCourse($ieCourse, "LESSON");
        return $this->render('student/course/show.html.twig', [
            'disableAllMenu' => false,
            'activeMenu' => ' course',
            'courseContents' => $courseContents,
            'course' => $ieCourse,
        ]);
    }

    /**
     * @Route("/download/course/content/file/{id}", name="app_student_course_download_file")
     */
    public function downloadFile(
        IeCourseContent $ieCourseContent
    ): Response {

        return $this->file('uploads/courses/content/' . $ieCourseContent->getFile());
    }
}
