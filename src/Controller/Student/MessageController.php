<?php

namespace App\Controller\Student;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/student", priority=10)
 */
class MessageController extends AbstractController
{

    /**
     * @Route("/message", name="app_student_message")
     */
    public function index(): Response
    {
        $user = $this->getUser();
        return $this->render('student/message/index.html.twig', [
            'disableAllMenu' => false,
            'activeMenu' => ' message',
            'user' => $user
        ]);
    }

    /**
     * @Route("/message/video/chat", name="app_student_message_video_chat")
     */
    public function videoChat(): Response
    {
        $user = $this->getUser();
        return $this->render('student/message/video-chat/index.html.twig', [
            'disableAllMenu' => false,
            'activeMenu' => ' message',
            'user' => $user
        ]);
    }

    /**
     * @Route("/message/video/chat/room/{key}", name="app_student_message_video_chat_room")
     */
    public function room($key): Response
    {
        $user = $this->getUser();
        $link = hex2bin($key);
        return $this->render('student/message/video-chat/index.html.twig', [
            'disableAllMenu' => false,
            'activeMenu' => ' message',
            'link' => $link,
            'user' => $user
        ]);
    }

    /**
     * @Route("/message/video/chat/justify", name="app_student_message_video_chat_key")
     */
    public function justify(Request $request): Response
    {
        $user = $this->getUser();
        $keywords = $request->request->get('keywords', '');
        return $this->redirectToRoute('app_student_message_video_chat_room', ['key' => bin2hex($keywords)]);
    }
}
