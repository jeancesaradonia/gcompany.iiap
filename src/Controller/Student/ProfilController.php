<?php

namespace App\Controller\Student;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/student", priority=10)
 */
class ProfilController extends AbstractController
{

    /**
     * @Route("/profil/show", name="app_student_profil_show")
     */
    public function show(): Response
    {
        $user = $this->getUser();
        return $this->render('student/profil/show.html.twig', [
            'disableAllMenu' => false,
            'activeMenu' => ' profil',
            'user' => $user
        ]);
    }
}
