<?php

namespace App\Controller\Student;

use App\Entity\IeCourse;
use App\Entity\IeTaught;
use App\Repository\IeVagueRepository;
use App\Repository\IeTaughtRepository;
use App\Repository\IeCourseContentRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/student/examen", priority=10)
 */
class ExamenController extends AbstractController
{

    /**
     * @Route("/", name="app_student_examen")
     */
    public function index(): Response
    {
        dd('examen');
        return $this->render('student/course/index.html.twig', [
            'disableAllMenu' => false,
            'activeMenu' => ' no',
        ]);
    }
}
