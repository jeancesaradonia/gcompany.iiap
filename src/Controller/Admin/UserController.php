<?php

namespace App\Controller\Admin;

// use ZipArchive;
use App\Entity\IeRole;
use App\Entity\IeUser;
use ZipStream\ZipStream;
use App\Entity\IeFaculty;
use App\Form\UserFormType;
use ZipStream\Option\Archive;
use App\Form\FilterStudentType;
use App\Security\EmailVerifier;
use App\Service\StudentService;
use App\Form\ValidationStudentType;
use Symfony\Component\Mime\Address;
use App\Entity\IeStudentInformation;
use App\Repository\IeRoleRepository;
use App\Repository\IeUserRepository;
use App\Repository\IeLevelRepository;
use App\Repository\IeVagueRepository;
use Symfony\Component\Form\FormError;
use App\Repository\IeFacultyRepository;
use App\Repository\IeSemesterRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\KernelInterface;
use App\Repository\IeStudentInformationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * @Route("/admin", priority=10)
 */
class UserController extends AbstractController
{
    private $emailVerifier;

    public function __construct(EmailVerifier $emailVerifier)
    {
        $this->emailVerifier = $emailVerifier;
    }

    private function getLastRoute(Request $request)
    {
        $referer = $request->headers->get('referer');
        $baseUrl = $request->getBaseUrl();
        return substr($referer, strpos($referer, $baseUrl) + strlen($baseUrl));
    }
    /**
     * @Route("/user", name="app_admin_user")
     */
    public function index(IeUserRepository $ieUserRepository, Request $request): Response
    {
        $users = $ieUserRepository->getUserByRoleType($request->query->get('type', 'ROLE_STUDENT'));
        return $this->render('admin/users/index.html.twig', compact('users'));
    }

    /**
     * @Route("/user/non-confirm", name="app_admin_user_non_confirm")
     */
    public function nonConfirmUser(IeUserRepository $ieUserRepository, Request $request): Response
    {
        $users = $ieUserRepository->getUserByRoleTypeNonConfirm($request->query->get('type', 'ROLE_STUDENT'));
        return $this->render('admin/users/non-confirm.html.twig', compact('users'));
    }

    /**
     * @Route("/user/non-verified", name="app_admin_user_non_verified")
     */
    public function nonVerifiedUser(IeUserRepository $ieUserRepository, Request $request): Response
    {
        $users = $ieUserRepository->getUserByRoleTypeNonVerified($request->query->get('type', 'ROLE_STUDENT'));
        return $this->render('admin/users/non-verified.html.twig', compact('users'));
    }

    /**
     * @Route("/user/{id<\d+>}/confirm", name="app_admin_user_confirm")
     */
    public function confirm(IeUser $ieUser, EntityManagerInterface $entityManagerInterface, Request $request): Response
    {
        $ieUser->setConfirmAt(new \Datetime('now'));
        $entityManagerInterface->flush();
        $this->addFlash('success', 'Confirmation d\'utilisateur : "' . $ieUser->getFullName() . '" avec succès');
        return $this->redirect($this->getLastRoute($request));
    }

    /**
     * @Route("/user/{id<\d+>}/verify", name="app_admin_user_verify")
     */
    public function verify(IeUser $ieUser, EntityManagerInterface $entityManagerInterface, Request $request): Response
    {
        $ieUser->setIsVerified(true);
        $entityManagerInterface->flush();
        $this->addFlash('success', 'Vérification d\'utilisateur : "' . $ieUser->getFullName() . '" avec succès');
        return $this->redirect($this->getLastRoute($request));
    }

    /**
     * @Route("/user/add", name="app_admin_user_add")
     */
    public function add(Request $request, UserPasswordHasherInterface $userPasswordHasher, EntityManagerInterface $entityManager, IeRoleRepository $ieRoleRepository): Response
    {
        $role = $request->query->get('type', 'ROLE_STUDENT');
        $user = new IeUser();
        $user->setIsDeleted(false);
        $user->addIeRole($ieRoleRepository->findOneByName($role));
        $user->setBirthDate(new \DateTime());
        $form = $this->createForm(UserFormType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager->persist($user);
            $entityManager->flush();

            $this->emailVerifier->sendEmailConfirmation(
                'app_verify_email',
                $user,
                (new TemplatedEmail())
                    ->from(new Address('iiap-education@gmail.com', 'IIAP EDUCATION'))
                    ->to($user->getEmail())
                    ->subject('Please Confirm your Email')
                    ->htmlTemplate('registration/confirmation_email.html.twig')
            );
            $this->addFlash('success', 'Création d\'une nouvelle compte avec succèss, une E-mail a été envoyé vers : ' . $user->getEmail() . ' pour la vérification de cette compte.');
            return $this->redirect($this->generateUrl('app_admin_user') . '?type=' . $request->query->get('type', 'ROLE_STUDENT'));
        }

        return $this->render('admin/users/add.html.twig', [
            'form' => $form->createView(),
        ]);

        return $this->render('admin/users/add.html.twig');
    }





    // REMOVE STUDENT

    /**
     * @Route("/user/student/{id<\d+>}/remove", name="app_admin_user_student_remove")
     */
    public function removeStudent(
        IeUser $student,
        EntityManagerInterface $entityManagerInterface,
        IeStudentInformationRepository $ieStudentInformationRepository
    ): Response {
        if ($student) {
            $studentInformation = $ieStudentInformationRepository->getInformationByUser($student);
            if ($studentInformation) {
                $entityManagerInterface->remove($studentInformation);
            }
            $entityManagerInterface->remove($student);
        }
        $entityManagerInterface->flush();
        return $this->render('admin/users/index.html.twig', [
            'users' => []
        ]);
    }





    // MENU INSCRIPTION

    /**
     * @Route("/user/student/download-data/{id<\d+>}", name="app_admin_user_student_download_data")
     */
    public function downloadData(
        IeUser $ieUser,
        Request $request,
        IeVagueRepository $ieVagueRepository,
        IeStudentInformationRepository $ieStudentInformationRepository,
        StudentService $studentService,
        EntityManagerInterface $entityManagerInterface,
        UserPasswordHasherInterface $userPasswordHasher,
        KernelInterface $appKernel
    ): Response {

        $studentInformation = $ieUser->getStudentInformation();

        $zip = new \ZipArchive;


        $tmp_file = 'uploads/studentZipInformations/' . $ieUser->getEmail() . '.zip';

        $filesystem = new Filesystem();
        if ($filesystem->exists($tmp_file)) {
            $filesystem->remove($tmp_file);
        }

        if ($zip->open($tmp_file,  \ZipArchive::CREATE)) {
            if ($studentInformation->getBirthCertificate()) {
                $zip->addFile('uploads/studentInformations/birthCertificates/' . $studentInformation->getBirthCertificate(), 'birthCertificates-' . $studentInformation->getBirthCertificate());
            }
            if ($studentInformation->getCinPhoto()) {
                $zip->addFile('uploads/studentInformations/cinPhotos/' . $studentInformation->getCinPhoto(), 'cinPhotos-' . $studentInformation->getCinPhoto());
            }
            if ($studentInformation->getCurriculumVitae()) {
                $zip->addFile('uploads/studentInformations/curriculumVitaes/' . $studentInformation->getCurriculumVitae(), 'curriculumVitaes-' . $studentInformation->getCurriculumVitae());
            }
            if ($studentInformation->getMotivationLetter()) {
                $zip->addFile('uploads/studentInformations/motivationLetters/' . $studentInformation->getMotivationLetter(), 'motivationLetters-' . $studentInformation->getMotivationLetter());
            }
            if ($studentInformation->getNoteOrCopyOfTheCertifiedDiploma()) {
                $zip->addFile('uploads/studentInformations/noteOrCopyOfTheCertifiedDiplomas/' . $studentInformation->getNoteOrCopyOfTheCertifiedDiploma(), 'noteOrCopyOfTheCertifiedDiplomas-' . $studentInformation->getNoteOrCopyOfTheCertifiedDiploma());
            }
            if ($studentInformation->getResidenceCertificate()) {
                $zip->addFile('uploads/studentInformations/residenceCertificates/' . $studentInformation->getResidenceCertificate(), 'residenceCertificates-' . $studentInformation->getResidenceCertificate());
            }
            $zip->close();

            $filesystem = new Filesystem();
            if ($filesystem->exists($tmp_file)) {
                return $this->file($tmp_file);
            } else {
                $this->addFlash('danger', 'Le dossier de ' . $ieUser->getEmail() . ' est vide ou possède quelque problème!');
            }
        }

        return $this->redirectToRoute('app_admin_user_student_registration_request_detail', ['id' => $ieUser->getId()]);
    }


    /**
     * @Route("/user/student/registration-request", name="app_admin_user_student_registration_request")
     */
    public function registrationRequest(IeUserRepository $ieUserRepository, Request $request): Response
    {
        $users = $ieUserRepository->getRegistrationRequest();
        return $this->render('admin/users/student/registration-request.html.twig', [
            'users' => $users,
            'activeMenu' => ' inscription'
        ]);
    }

    /**
     * @Route("/user/student/registration-request/{id<\d+>}/detail", name="app_admin_user_student_registration_request_detail")
     */
    public function registrationRequestDetail(
        IeUser $ieUser,
        Request $request,
        IeVagueRepository $ieVagueRepository,
        IeStudentInformationRepository $ieStudentInformationRepository,
        StudentService $studentService,
        EntityManagerInterface $entityManagerInterface,
        UserPasswordHasherInterface $userPasswordHasher
    ): Response {


        $studentInformation = $ieUser->getStudentInformation();
        $errorValidation = false;

        $validationStudentForm = $this->createForm(ValidationStudentType::class, $studentInformation, [
            'level' => $studentInformation->getLevel(),
            // 'idNumber' => $studentInformation->getId(),
        ]);

        $validationStudentForm->handleRequest($request);
        if ($validationStudentForm->isSubmitted() && $validationStudentForm->isValid()) {

            $data = $request->request->get($validationStudentForm->getName());
            $matriculeNumber = $studentService->generateMatriculeNumber($studentInformation, sprintf('%03d', (int)$data['numero']));
            $studentInformationExisted = $ieStudentInformationRepository->findByMatriculeNumber($matriculeNumber);
            if ($studentInformationExisted) {
                $this->addFlash('danger', 'Cette numero est déjà prise par un(e) étudiant(e) qui a un numero de matricule : ' . $matriculeNumber);
                $error = new FormError("Valeur déja prise");
                $validationStudentForm->get('numero')->addError($error);
                $errorValidation = true;
            } else {
                $this->addFlash('success', 'Ok : ' . $matriculeNumber);
                $studentInformation->setMatriculeNumber($matriculeNumber);

                // modification de mot de passe
                $ieUser->setPassword(
                    $userPasswordHasher->hashPassword(
                        $ieUser,
                        $ieUser->getPhoneNumber() . '-i.e'
                    )
                );
                $ieUser->setConfirmAt(new \Datetime('now'));
                $entityManagerInterface->flush();
                // On envoie un E-mail ici

                $this->emailVerifier->sendEmailConfirmation(
                    'app_verify_email',
                    $ieUser,
                    (new TemplatedEmail())
                        ->from(new Address('iiap-education@gmail.com', 'IIAP EDUCATION'))
                        ->to($ieUser->getEmail())
                        ->subject('Demande d\inscription accepté')
                        ->htmlTemplate('registration/confirmation_email.html.twig')
                );
                $this->addFlash('success', 'Demande d\'inscription accepté : ' . $ieUser->getFullName());
                return $this->redirectToRoute('app_admin_user_student_registration_request');
            }
        }
        return $this->render('admin/users/student/registration-request-detail.html.twig', [
            'user' => $ieUser,
            'errorValidation' => $errorValidation,
            'validationStudentForm' => $validationStudentForm->createView(),
            'activeMenu' => ' inscription'
        ]);
    }







    // MENU ÉTUDIANT
    /**
     * @Route("/user/student/faculty", name="app_admin_user_show")
     */
    public function userStudentShow(IeFacultyRepository $ieFacultyRepository)
    {
        $faculties = $ieFacultyRepository->findAll();
        return $this->render('admin/users/student/faculty.html.twig', [
            'faculties' => $faculties,
            'activeMenu' => ' étudiant'
        ]);
    }

    /**
     * @Route("/user/student/faculty/{id<\d+>}", name="app_admin_user_faculty_show")
     */
    public function userStudentFacultyShow(
        IeFaculty $ieFaculty,
        Request $request,
        IeStudentInformationRepository $ieStudentInformationRepository,
        IeFacultyRepository $ieFacultyRepository,
        IeLevelRepository $ieLevelRepository,
        IeSemesterRepository $ieSemesterRepository,
        IeVagueRepository $ieVagueRepository
    ) {
        $openCard = false;
        $filterStudentForm = $this->createForm(FilterStudentType::class);
        $students = $ieStudentInformationRepository->getRegistratedStudentByFaculty($ieFaculty);
        $filterStudentForm->handleRequest($request);
        if ($filterStudentForm->isSubmitted()) {
            $ieStudentInformation = new IeStudentInformation();
            $data = $request->request->get($filterStudentForm->getName());
            $ieStudentInformation->setType($data['type']);
            $ieStudentInformation->setFaculty($ieFaculty);
            $ieStudentInformation->setLevel($ieLevelRepository->findOneById($data['level']));
            $ieStudentInformation->setVague($ieVagueRepository->findOneById($data['vague']));
            $ieStudentInformation->setSemester($ieSemesterRepository->findOneById($data['semester']));
            $students = $ieStudentInformationRepository->getFilterStudentInformation($ieStudentInformation);
            $openCard = true;
        }
        return $this->render('admin/users/student/faculty-show.html.twig', [
            'faculty' => $ieFaculty,
            'students' => $students,
            'filterStudentForm' => $filterStudentForm->createView(),
            'openCard' => $openCard,
            'activeMenu' => ' étudiant'
        ]);
    }





    // MENU STATUS DE PAIMENT
    /**
     * @Route("/user/student/payment-status", name="app_admin_user_payment_status")
     */
    public function userStudentPaymentStatus(
        IeStudentInformationRepository $ieStudentInformationRepository,
        Request $request,
        IeFacultyRepository $ieFacultyRepository,
        IeLevelRepository $ieLevelRepository,
        IeSemesterRepository $ieSemesterRepository,
        IeVagueRepository $ieVagueRepository
    ) {
        $openCard = false;
        $filterStudentForm = $this->createForm(FilterStudentType::class, null, [
            'faculty' => 'allow'
        ]);
        $ieStudentInformation = new IeStudentInformation();
        $students = $ieStudentInformationRepository->getPaymentStatusStudent($ieStudentInformation);
        $filterStudentForm->handleRequest($request);
        if ($filterStudentForm->isSubmitted()) {
            $ieStudentInformation = new IeStudentInformation();
            $data = $request->request->get($filterStudentForm->getName());
            $ieStudentInformation->setType($data['type']);
            $ieStudentInformation->setFaculty($ieFacultyRepository->findOneById($data['faculty']));
            $ieStudentInformation->setLevel($ieLevelRepository->findOneById($data['level']));
            $ieStudentInformation->setVague($ieVagueRepository->findOneById($data['vague']));
            $ieStudentInformation->setSemester($ieSemesterRepository->findOneById($data['semester']));
            $students = $ieStudentInformationRepository->getPaymentStatusStudent($ieStudentInformation);
            $openCard = true;
        }
        return $this->render('admin/users/student/payment-status.html.twig', [
            'students' => $students,
            'openCard' => $openCard,
            'filterStudentForm' => $filterStudentForm->createView(),
            'activeMenu' => ' status-de-paiment'
        ]);
    }




    // MENU STATUS DES ÉTUDIANTS
    /**
     * @Route("/user/student/status", name="app_admin_user_status")
     */
    public function userStudentStatus(
        IeStudentInformationRepository $ieStudentInformationRepository,
        Request $request,
        IeFacultyRepository $ieFacultyRepository,
        IeLevelRepository $ieLevelRepository,
        IeSemesterRepository $ieSemesterRepository,
        IeVagueRepository $ieVagueRepository
    ) {
        $openCard = false;
        $filterStudentForm = $this->createForm(FilterStudentType::class, null, [
            'faculty' => 'allow'
        ]);
        $ieStudentInformation = new IeStudentInformation();
        $students = $ieStudentInformationRepository->getPaymentStatusStudent($ieStudentInformation);
        $filterStudentForm->handleRequest($request);
        if ($filterStudentForm->isSubmitted()) {
            $ieStudentInformation = new IeStudentInformation();
            $data = $request->request->get($filterStudentForm->getName());
            $ieStudentInformation->setType($data['type']);
            $ieStudentInformation->setFaculty($ieFacultyRepository->findOneById($data['faculty']));
            $ieStudentInformation->setLevel($ieLevelRepository->findOneById($data['level']));
            $ieStudentInformation->setVague($ieVagueRepository->findOneById($data['vague']));
            $ieStudentInformation->setSemester($ieSemesterRepository->findOneById($data['semester']));
            $students = $ieStudentInformationRepository->getPaymentStatusStudent($ieStudentInformation);
            $openCard = true;
        }
        return $this->render('admin/users/student/status.html.twig', [
            'students' => $students,
            'openCard' => $openCard,
            'filterStudentForm' => $filterStudentForm->createView(),
            'activeMenu' => ' status'
        ]);
    }
}
