<?php

namespace App\Controller\Admin;

use App\Entity\IeVague;
use App\Entity\IeCourse;
use App\Entity\IeCourseContent;
use App\Entity\IeTaught;
use App\Form\CourseType;
use App\Form\TaughtType;
use App\Entity\IeTeachingUnit;
use App\Form\CourseContentType;
use App\Form\StartCourseType;
use App\Form\TeachingUnitType;
use App\Form\VagueType;
use App\Repository\IeCourseContentRepository;
use App\Repository\IeVagueRepository;
use Symfony\Component\Form\FormError;
use App\Repository\IeCourseRepository;
use App\Repository\IeTaughtRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\IeTeachingUnitRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @Route("/admin/course", priority=10)
 */
class CourseController extends AbstractController
{
    /**
     * @Route("/teaching-unit", name="app_admin_course_teaching_unit")
     */
    public function index(Request $request, IeTeachingUnitRepository $ieTeachingUnitRepository, EntityManagerInterface $entityManagerInterface): Response
    {
        $teachingUnits = $ieTeachingUnitRepository->findAll();

        $teachingUnit = new IeTeachingUnit();
        $form = $this->createForm(TeachingUnitType::class, $teachingUnit);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManagerInterface->persist($teachingUnit);
            $entityManagerInterface->flush();
            $this->addFlash('success', 'Ajout avec succèss');
            return $this->redirectToRoute('app_admin_course_teaching_unit');
        }
        return $this->render('admin/courses/teaching-unit.html.twig', [
            'teachingUnits' => $teachingUnits,
            'activeMenu' => ' teaching-unit',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/show/{slug}", name="app_admin_course_show_detail")
     */
    public function showDetail(
        IeCourse $course,
        Request $request,
        EntityManagerInterface $entityManagerInterface,
        IeCourseContentRepository $ieCourseContentRepository
    ): Response {
        $courseContent = new IeCourseContent();
        $form = $this->createForm(CourseContentType::class, $courseContent);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $courseContent->setCourse($course);
            $entityManagerInterface->persist($courseContent);
            $entityManagerInterface->flush();
            $this->addFlash('success', 'Ajout du contenu du cours ' . $course->getTitle() . ' [type:' . $courseContent->getCourseType() . ']avec succèss');
            return $this->redirectToRoute('app_admin_course_show_detail', ['slug' => $course->getSlug()]);
        }
        $lessons = $ieCourseContentRepository->getCourse($course, "LESSON");
        $exercices = $ieCourseContentRepository->getCourse($course, "EXERCICE");
        $corrections = $ieCourseContentRepository->getCourse($course, "CORRECTION");
        return $this->render('admin/courses/show-detail.html.twig', [
            'activeMenu' => ' list',
            'form' => $form->createView(),
            'course' => $course,
            'lessons' => $lessons,
            'exercices' => $exercices,
            'corrections' => $corrections,
        ]);
    }



    /**
     * @Route("/download/course/content/file/{id}", name="app_admin_course_download_file")
     */
    public function downloadFile(
        IeCourseContent $ieCourseContent
    ): Response {

        return $this->file('uploads/courses/content/' . $ieCourseContent->getFile());
    }

    /**
     * @Route("/list", name="app_admin_course_list")
     */
    public function list(Request $request, IeCourseRepository $ieCourseRepository, EntityManagerInterface $entityManagerInterface, SluggerInterface $sluggerInterface): Response
    {
        $cours = $ieCourseRepository->findAll();

        $course = new IeCourse();
        $form = $this->createForm(CourseType::class, $course);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $course->setSlug($sluggerInterface->slug($course->getTitle()));
            $entityManagerInterface->persist($course);
            $entityManagerInterface->flush();
            $this->addFlash('success', 'Ajout matière avec succèss');
            return $this->redirectToRoute('app_admin_course_list');
        }
        return $this->render('admin/courses/list.html.twig', [
            'courses' => $cours,
            'activeMenu' => ' list',
            'form' => $form->createView(),
            'openModal' => false,
        ]);
    }

    /**
     * @Route("/teaching-course", name="app_admin_course_teaching_course")
     */
    public function teachingCourse(Request $request, IeTaughtRepository $ieTaughtRepository, EntityManagerInterface $entityManagerInterface): Response
    {
        $taugths = $ieTaughtRepository->findAll();

        $taugth = new IeTaught();
        $form = $this->createForm(TaughtType::class, $taugth);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManagerInterface->persist($taugth);
            $entityManagerInterface->flush();
            $this->addFlash('success', 'Ajout Formation avec succès');
            return $this->redirectToRoute('app_admin_course_teaching_course');
        }
        return $this->render('admin/courses/teaching-course.html.twig', [
            'taugths' => $taugths,
            'openCard' => false,
            'activeMenu' => ' teaching-course',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/starting", name="app_admin_course_starting")
     */
    public function starting(Request $request, IeVagueRepository $ieVagueRepository, EntityManagerInterface $entityManagerInterface): Response
    {

        $openModal = false;
        $ieVague = new IeVague();
        $form = $this->createForm(VagueType::class, $ieVague);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManagerInterface->persist($ieVague);
            $entityManagerInterface->flush();
            $this->addFlash('success', 'Ajout d\'une nouvelle vague avec succès');
            return $this->redirectToRoute('app_admin_course_starting');
        }
        $startedVagues = $ieVagueRepository->getStartedVague();
        return $this->render('admin/courses/starting.html.twig', [
            'activeMenu' => ' ',
            'startedVagues' => $startedVagues,
            'form' => $form->createView(),
            'openModal' => $openModal,
        ]);
    }

    /**
     * @Route("/starting/{id<\d+>}", name="app_admin_course_starting_edit")
     */
    public function edit(Request $request, IeVague $ieVague, EntityManagerInterface $entityManagerInterface): Response
    {

        $ieVague = new IeVague();
        $form = $this->createForm(VagueType::class, $ieVague);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManagerInterface->persist($ieVague);
            $entityManagerInterface->flush();
            $this->addFlash('success', 'Ajout d\'une nouvelle date d\'entrée avec succès');
            return $this->redirectToRoute('app_admin_course_starting');
        }
        return $this->render('admin/courses/edit-vague.html.twig', [
            'activeMenu' => ' ',
            'startedVagues' => $ieVague,
            'form' => $form->createView(),
            'openModal' => false,
        ]);
    }
}
