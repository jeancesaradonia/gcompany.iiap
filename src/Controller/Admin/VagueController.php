<?php

namespace App\Controller\Admin;

use App\Entity\IeVague;
use App\Entity\IeCourse;
use App\Entity\IeTaught;
use App\Form\CourseType;
use App\Form\TaughtType;
use App\Entity\IeTeachingUnit;
use App\Form\StartCourseType;
use App\Form\TeachingUnitType;
use App\Form\VagueType;
use App\Repository\IeVagueRepository;
use Symfony\Component\Form\FormError;
use App\Repository\IeCourseRepository;
use App\Repository\IeTaughtRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\IeTeachingUnitRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/vague", priority=10)
 */
class VagueController extends AbstractController
{
    /**
     * @Route("/list", name="app_admin_vague")
     */
    public function show(Request $request, IeVagueRepository $ieVagueRepository, EntityManagerInterface $entityManagerInterface): Response
    {
        $openModal = false;
        $ieVague = new IeVague();
        $form = $this->createForm(VagueType::class, $ieVague);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManagerInterface->persist($ieVague);
            $entityManagerInterface->flush();
            $this->addFlash('success', 'Ajout d\'une nouvelle vague avec succès');
            return $this->redirectToRoute('app_admin_vague');
        }
        $startedVagues = $ieVagueRepository->getStartedVague();
        return $this->render('admin/vagues/list.html.twig', [
            'activeMenu' => ' ',
            'startedVagues' => $startedVagues,
            'form' => $form->createView(),
            'openModal' => $openModal,
        ]);
    }

    /**
     * @Route("/{id<\d+>}/edit", name="app_admin_vague_edit")
     */
    public function edit(Request $request, IeVague $ieVague, EntityManagerInterface $entityManagerInterface): Response
    {
        $form = $this->createForm(VagueType::class, $ieVague);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManagerInterface->persist($ieVague);
            $entityManagerInterface->flush();
            $this->addFlash('success', 'Modification avec succès');
            return $this->redirectToRoute('app_admin_vague');
        }
        return $this->render('admin/vagues/edit.html.twig', [
            'activeMenu' => ' ',
            'form' => $form->createView(),
            'vague' => $ieVague,
            'openModal' => false,
        ]);
    }


    /**
     * @Route("/get-start-date/{id<\d+>}/", name="app_admin_vague_get_start_date")
     */
    public function getStartDate(IeVague $ieVague): Response
    {
        return $this->json([
            'vague' => $ieVague->getVNumber(),
            'startedAt' => $ieVague->getStartAt(),
        ]);
    }
}
