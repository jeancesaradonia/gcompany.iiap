<?php

namespace App\Controller\Admin;

use App\Entity\IeRole;
use App\Entity\IeUser;
use App\Form\UserFormType;
use App\Security\EmailVerifier;
use Symfony\Component\Mime\Address;
use App\Repository\IeRoleRepository;
use App\Repository\IeUserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * @Route("/admin", priority=10)
 */
class IeUserController extends AbstractController
{
    private $emailVerifier;

    public function __construct(EmailVerifier $emailVerifier)
    {
        $this->emailVerifier = $emailVerifier;
    }

    private function getLastRoute(Request $request)
    {
        $referer = $request->headers->get('referer');
        $baseUrl = $request->getBaseUrl();
        return substr($referer, strpos($referer, $baseUrl) + strlen($baseUrl));
    }
    /**
     * @Route("/user", name="app_admin_user")
     */
    public function index(IeUserRepository $ieUserRepository, Request $request): Response
    {
        $users = $ieUserRepository->getUserByRoleType($request->query->get('type', 'ROLE_STUDENT'));
        $usersNonConfirm = $ieUserRepository->getUserByRoleTypeNonConfirm($request->query->get('type', 'ROLE_STUDENT'));
        $usersNonVerified = $ieUserRepository->getUserByRoleTypeNonVerified($request->query->get('type', 'ROLE_STUDENT'));
        return $this->render('admin/users/index.html.twig', compact('users', 'usersNonConfirm', 'usersNonVerified'));
    }

    /**
     * @Route("/user/non-confirm", name="app_admin_user_non_confirm")
     */
    public function nonConfirmUser(IeUserRepository $ieUserRepository, Request $request): Response
    {
        $users = $ieUserRepository->getUserByRoleTypeNonConfirm($request->query->get('type', 'ROLE_STUDENT'));
        return $this->render('admin/users/non-confirm.html.twig', compact('users'));
    }

    /**
     * @Route("/user/non-verified", name="app_admin_user_non_verified")
     */
    public function nonVerifiedUser(IeUserRepository $ieUserRepository, Request $request): Response
    {
        $users = $ieUserRepository->getUserByRoleTypeNonVerified($request->query->get('type', 'ROLE_STUDENT'));
        return $this->render('admin/users/non-verified.html.twig', compact('users'));
    }

    /**
     * @Route("/user/{id<\d+>}/confirm", name="app_admin_user_confirm")
     */
    public function confirm(IeUser $ieUser, EntityManagerInterface $entityManagerInterface, Request $request): Response
    {
        $ieUser->setConfirmAt(new \Datetime('now'));
        $entityManagerInterface->flush();
        $this->addFlash('success', 'Confirmation d\'utilisateur : "' . $ieUser->getFullName() . '" avec succès');
        return $this->redirect($this->getLastRoute($request));
    }

    /**
     * @Route("/user/{id<\d+>}/verify", name="app_admin_user_verify")
     */
    public function verify(IeUser $ieUser, EntityManagerInterface $entityManagerInterface, Request $request): Response
    {
        $ieUser->setIsVerified(true);
        $entityManagerInterface->flush();
        $this->addFlash('success', 'Vérification d\'utilisateur : "' . $ieUser->getFullName() . '" avec succès');
        return $this->redirect($this->getLastRoute($request));
    }

    /**
     * @Route("/user/add", name="app_admin_user_add")
     */
    public function add(Request $request, UserPasswordHasherInterface $userPasswordHasher, EntityManagerInterface $entityManager, IeRoleRepository $ieRoleRepository): Response
    {
        $role = $request->query->get('type', 'ROLE_STUDENT');
        $user = new IeUser();
        $user->setIsDeleted(false);
        $user->addIeRole($ieRoleRepository->findOneByName($role));
        $user->setBirthDate(new \DateTime());
        $form = $this->createForm(UserFormType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager->persist($user);
            $entityManager->flush();

            $this->emailVerifier->sendEmailConfirmation(
                'app_verify_email',
                $user,
                (new TemplatedEmail())
                    ->from(new Address('admin@iiap.education', 'IIAP EDUCATION'))
                    ->to($user->getEmail())
                    ->subject('Please Confirm your Email')
                    ->htmlTemplate('registration/confirmation_email.html.twig')
            );
            $this->addFlash('success', 'Création d\'une nouvelle compte avec succèss, une E-mail a été envoyé vers : ' . $user->getEmail() . ' pour la vérification de cette compte.');
            return $this->redirect($this->generateUrl('app_admin_user') . '?type=' . $request->query->get('type', 'ROLE_STUDENT'));
        }

        return $this->render('admin/users/add.html.twig', [
            'form' => $form->createView(),
        ]);

        return $this->render('admin/users/add.html.twig');
    }
}
