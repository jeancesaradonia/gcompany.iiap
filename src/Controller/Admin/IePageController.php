<?php

namespace App\Controller\Admin;

use App\Entity\IePage;
use App\Form\IePageType;
use App\Repository\IePageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Twig\Environment;

/**
 * @Route("/admin", priority=10)
 */
class IePageController extends AbstractController
{
    /**
     * @Route("/page", name="app_admin_page")
     */
    public function index(IePageRepository $iePageRepository): Response
    {
        return $this->render('admin/pages/index.html.twig', [
            'pages' => $iePageRepository->findNotDeletedPage()
        ]);
    }
    /**
     * @Route("/page/on", name="app_admin_page_on")
     */
    public function on(IePageRepository $iePageRepository): Response
    {
        return $this->render('admin/pages/on.html.twig', [
            'pages' => $iePageRepository->findNotDeletedPageOn()
        ]);
    }
    /**
     * @Route("/page/off", name="app_admin_page_off")
     */
    public function off(IePageRepository $iePageRepository): Response
    {
        return $this->render('admin/pages/off.html.twig', [
            'pages' => $iePageRepository->findNotDeletedPageOff()
        ]);
    }

    /**
     * @Route("/page/render-view/{view}", name="app_admin_page_render_iframe", requirements={"view"=".+"})
     */
    public function render_iframe($view): Response
    {
        return $this->render($view . '.twig');
    }

    /**
     * @Route("/page/{id}/view", name="app_admin_page_view")
     */
    public function viewPage(IePage $iePage, Environment $twig): Response
    {
        $noView = false;
        if (!$twig->getLoader()->exists($iePage->getView() . '.twig')) $noView = true;
        return $this->render('admin/pages/view.html.twig', compact('iePage', 'noView'));
    }

    /**
     * @Route("/page/add", name="app_admin_page_add")
     */
    public function add(Request $request, EntityManagerInterface $em): Response
    {
        $iePage = new IePage();
        $iePage->setIsDeleted(false);
        $form = $this->createForm(IePageType::class, $iePage);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($iePage);
            $em->flush();
            $this->addFlash('success', 'Ajout d\'une nouvelle page ' . $iePage->getTitle() . ' avec succès');
            return $this->redirectToRoute('app_admin_page');
        }
        return $this->renderForm('admin/pages/add.html.twig', compact('form'));
    }

    /**
     * @Route("/page/{id}/edit", name="app_admin_page_edit")
     */
    public function edit(IePage $iePage, Request $request, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(IePageType::class, $iePage);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            $this->addFlash('success', 'Modification de la page ' . $iePage->getTitle() . ' avec succès');
            return $this->redirectToRoute('app_admin_page');
        }
        return $this->renderForm('admin/pages/edit.html.twig', compact('iePage', 'form'));
    }

    /**
     * @Route("/page/{id}/toggle/active", name="app_admin_page_toggle")
     */
    public function toogle(IePage $iePage, EntityManagerInterface $em): Response
    {
        if ($iePage->isIsActive()) {
            $this->addFlash('danger', 'Désactivation de la page "' . $iePage->getTitle() . '" avec succèss');
        } else {
            $this->addFlash('success', 'L\'activation de la page "' . $iePage->getTitle() . '" avec succèss');
        }
        $iePage->setIsActive(!$iePage->isIsActive());
        $em->flush();
        return $this->redirectToRoute('app_admin_page');
    }

    /**
     * @Route("/page/{id}/delete", name="app_admin_page_delete")
     */
    public function delete(IePage $iePage, EntityManagerInterface $em): Response
    {
        $iePage->setIsDeleted(true);
        $em->flush();
        $this->addFlash('success', 'Suppression de la page "' . $iePage->getTitle() . '" avec succèss');
        return $this->redirectToRoute('app_admin_page');
    }
}
