<?php

namespace App\Controller\Admin;

use App\Entity\IeExam;
use App\Form\ExamType;
use App\Repository\IeExamRepository;
use App\Repository\IeUserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin", priority=10)
 */
class ExamenController extends AbstractController
{
    /**
     * @Route("/examen/add", name="app_admin_examen_add")
     */
    public function index(Request $request, EntityManagerInterface $entityManagerInterface, IeExamRepository $ieExamRepository): Response
    {
        $exams = $ieExamRepository->findAll();

        $exam = new IeExam();
        $form = $this->createForm(ExamType::class, $exam);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManagerInterface->persist($exam);
            $entityManagerInterface->flush();
            $this->addFlash('success', 'Ajout avec succèss');
            return $this->redirectToRoute('app_admin_examen_add');
        }

        return $this->render('admin/examens/add.html.twig', [
            'activeMenu' => ' examen-add',
            'exams' => $exams,
            'form' => $form->createView(),
        ]);
    }
}
