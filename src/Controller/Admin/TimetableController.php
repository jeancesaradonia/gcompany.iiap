<?php

namespace App\Controller\Admin;

use App\Entity\IeLevel;
use App\Entity\IeTimetable;
use App\Form\TimetableType;
use App\Form\TimetableFilterType;
use App\Repository\IeFacultyRepository;
use App\Repository\IeLevelRepository;
use App\Repository\IeSemesterRepository;
use App\Repository\IeTimetableRepository;
use App\Repository\IeVagueRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/timetable", priority=10)
 */
class TimetableController extends AbstractController
{
    /**
     * @Route("/", name="app_admin_timetable")
     */
    public function index(IeFacultyRepository $ieFacultyRepository, Request $request): Response
    {
        $randomFaculty = $ieFacultyRepository->findAll()[0];
        $timetable = new IeTimetable();
        $timetable->setFaculty($randomFaculty);
        $form = $this->createForm(TimetableFilterType::class, $timetable);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
        }
        return $this->render('admin/timetables/index.html.twig', [
            'activeMenu' => ' timetable',
            'form' => $form->createView(),
            'faculty' => $timetable->getFaculty(),
            'timetable' => $timetable,
        ]);
    }

    /**
     * @Route("/get-data", name="app_admin_timetable_get_data")
     */
    public function getData(
        IeTimetableRepository $ieTimetableRepository,
        Request $request,
        IeFacultyRepository $ieFacultyRepository,
        IeLevelRepository $ieLevelRepository,
        IeVagueRepository $ieVagueRepository,
        IeSemesterRepository $ieSemesterRepository
    ): Response {
        $target = $request->request->get('target', '');
        $faculty = $ieFacultyRepository->findOneById($request->request->get('faculty', ''));
        $level = $ieFacultyRepository->findOneById($request->request->get('level', ''));
        $vague = $ieFacultyRepository->findOneById($request->request->get('vague', ''));
        $semester = $ieFacultyRepository->findOneById($request->request->get('semester', ''));

        $filter = compact('target', 'faculty', 'level', 'vague', 'semester');

        $timetables = $ieTimetableRepository->filter($filter);
        $data = [];
        foreach ($timetables as $timetable) {
            array_push($data, [
                'title' => $timetable->getTitle(),
                'start' => $timetable->getStartAt(),
                'end' => $timetable->getEndAt(),
                'backgroundColor' => $timetable->getColor(),
                'borderColor' => $timetable->getColor(),
            ]);
        }
        return $this->json($data);
    }


    /**
     * @Route("/add", name="app_admin_timetable_add")
     */
    public function add(Request $request, EntityManagerInterface $entityManagerInterface): Response
    {
        $timetable = new IeTimetable();
        $form = $this->createForm(TimetableType::class, $timetable);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManagerInterface->persist($timetable);
            $entityManagerInterface->flush();
            $this->addFlash('success', 'Ajout d\'une nouvelle emploi du temps avec succès');
            return $this->redirectToRoute('app_admin_timetable');
        }

        return $this->render('admin/timetables/add.html.twig', [
            'activeMenu' => ' timetable',
            'form' => $form->createView(),
        ]);
    }
}
