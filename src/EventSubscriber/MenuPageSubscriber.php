<?php

namespace App\EventSubscriber;

use Twig\Environment;
use App\Repository\IePageRepository;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class MenuPageSubscriber implements EventSubscriberInterface
{
    private $twig;
    private $iePageRepository;
    public function __construct(Environment $twig, IePageRepository $iePageRepository)
    {
        $this->twig = $twig;
        $this->iePageRepository = $iePageRepository;
    }
    public function onControllerEvent(ControllerEvent  $event)
    {
        $this->twig->addGlobal('allPages', $this->iePageRepository->getPages());
    }

    public static function getSubscribedEvents()
    {
        return [
            ControllerEvent::class => 'onControllerEvent',
        ];
    }
}
