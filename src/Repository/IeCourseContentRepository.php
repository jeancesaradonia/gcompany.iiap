<?php

namespace App\Repository;

use App\Entity\IeCourseContent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<IeCourseContent>
 *
 * @method IeCourseContent|null find($id, $lockMode = null, $lockVersion = null)
 * @method IeCourseContent|null findOneBy(array $criteria, array $orderBy = null)
 * @method IeCourseContent[]    findAll()
 * @method IeCourseContent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IeCourseContentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IeCourseContent::class);
    }

    public function add(IeCourseContent $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(IeCourseContent $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return IeCourseContent[] Returns an array of IeCourseContent objects
     */
    public function getCourse($course, $courseType): array
    {
        return $this->createQueryBuilder('i')
            ->innerJoin('i.course', 'c', 'WITH', 'c = :course')
            ->setParameter('course', $course)
            ->andWhere('i.courseType = :courseType')
            ->setParameter('courseType', $courseType)
            ->orderBy('i.updatedAt', 'DESC')
            ->getQuery()
            ->getResult();
    }


    //    public function findOneBySomeField($value): ?IeCourseContent
    //    {
    //        return $this->createQueryBuilder('i')
    //            ->andWhere('i.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
