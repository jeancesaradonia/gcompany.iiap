<?php

namespace App\Repository;

use App\Entity\IeTeacherInformation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<IeTeacherInformation>
 *
 * @method IeTeacherInformation|null find($id, $lockMode = null, $lockVersion = null)
 * @method IeTeacherInformation|null findOneBy(array $criteria, array $orderBy = null)
 * @method IeTeacherInformation[]    findAll()
 * @method IeTeacherInformation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IeTeacherInformationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IeTeacherInformation::class);
    }

    public function add(IeTeacherInformation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(IeTeacherInformation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return IeTeacherInformation[] Returns an array of IeTeacherInformation objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('i.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?IeTeacherInformation
//    {
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
