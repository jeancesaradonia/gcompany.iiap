<?php

namespace App\Repository;

use App\Entity\IeStudentInformation;
use App\Entity\IeTimetable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<IeTimetable>
 *
 * @method IeTimetable|null find($id, $lockMode = null, $lockVersion = null)
 * @method IeTimetable|null findOneBy(array $criteria, array $orderBy = null)
 * @method IeTimetable[]    findAll()
 * @method IeTimetable[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IeTimetableRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IeTimetable::class);
    }

    public function add(IeTimetable $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(IeTimetable $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return IeTimetable[] Returns an array of IeTimetable objects
     */
    public function filter($filter): array
    {
        $query = $this->createQueryBuilder('tt')
            ->innerJoin('tt.faculty', 'f', 'WITH', 'f = :faculty')
            ->setParameter('faculty', $filter['faculty'])
            ->andWhere('tt.target = :target')
            ->setParameter('target', $filter['target']);

        if ($filter['level']) {
            $query->innerJoin('tt.level', 'l', 'WITH', 'l = :level')
                ->setParameter('level', $filter['level']);
        }

        if ($filter['vague']) {
            $query->innerJoin('tt.vague', 'v', 'WITH', 'v = :vague')
                ->setParameter('vague', $filter['vague']);
        }
        if ($filter['semester']) {
            $query->innerJoin('tt.semester', 's', 'WITH', 's = :semester')
                ->setParameter('semester', $filter['semester']);
        }
        return
            $query
            ->orderBy('tt.id', 'ASC')
            ->getQuery()
            ->getResult();
    }



    /**
     * @return IeTimetable[] Returns an array of IeTimetable objects
     */
    public function getTimetables(IeStudentInformation $studentInformation): array
    {
        return $this->createQueryBuilder('tt')
            ->innerJoin('tt.faculty', 'f', 'WITH', 'f = :faculty')
            ->setParameter('faculty', $studentInformation->getFaculty())
            ->innerJoin('tt.level', 'l', 'WITH', 'l = :level')
            ->setParameter('level', $studentInformation->getLevel())
            ->innerJoin('tt.vague', 'v', 'WITH', 'v = :vague')
            ->setParameter('vague', $studentInformation->getVague())
            ->andWhere('tt.target = :target')
            ->setParameter('target', $studentInformation->getFormationType())
            ->orderBy('tt.id', 'ASC')
            ->getQuery()
            ->getResult();
    }


    //    public function findOneBySomeField($value): ?IeTimetable
    //    {
    //        return $this->createQueryBuilder('i')
    //            ->andWhere('i.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
