<?php

namespace App\Repository;

use App\Entity\IePage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<IePage>
 *
 * @method IePage|null find($id, $lockMode = null, $lockVersion = null)
 * @method IePage|null findOneBy(array $criteria, array $orderBy = null)
 * @method IePage[]    findAll()
 * @method IePage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IePageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IePage::class);
    }

    public function add(IePage $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(IePage $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return IePage[] Returns an array of IePage objects
     */
    public function findNotDeletedPage(): array
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.isDeleted = 0')
            ->orderBy('i.menuOrder', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return IePage[] Returns an array of IePage objects
     */
    public function findNotDeletedPageOn(): array
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.isDeleted = 0')
            ->andWhere('i.isActive = 1')
            ->orderBy('i.menuOrder', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return IePage[] Returns an array of IePage objects
     */
    public function findNotDeletedPageOff(): array
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.isDeleted = 0')
            ->andWhere('i.isActive = 0')
            ->orderBy('i.menuOrder', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return IePage[] Returns an array of IePage objects
     */
    public function getPages(): array
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.isDeleted = 0')
            ->andWhere('i.isActive = 1')
            ->orderBy('i.menuOrder', 'ASC')
            ->getQuery()
            ->getResult();
    }

    //    public function findOneBySomeField($value): ?IePage
    //    {
    //        return $this->createQueryBuilder('i')
    //            ->andWhere('i.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
