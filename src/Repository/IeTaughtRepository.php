<?php

namespace App\Repository;

use App\Entity\IeStudentInformation;
use App\Entity\IeTaught;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<IeTaught>
 *
 * @method IeTaught|null find($id, $lockMode = null, $lockVersion = null)
 * @method IeTaught|null findOneBy(array $criteria, array $orderBy = null)
 * @method IeTaught[]    findAll()
 * @method IeTaught[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IeTaughtRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IeTaught::class);
    }

    public function add(IeTaught $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(IeTaught $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return IeTaught[] Returns an array of IeTaught objects
     */
    public function getAvalaibleCourse(IeStudentInformation $studentInformation, $month): array
    {
        return $this->createQueryBuilder('iet')
            ->innerJoin('iet.semester', 's', 'WITH', 's = :semester')
            ->setParameter('semester', $studentInformation->getSemester())
            ->innerJoin('iet.faculty', 'f', 'WITH', 'f = :faculty')
            ->setParameter('faculty', $studentInformation->getFaculty())
            ->andWhere('iet.month <= :month')
            ->setParameter('month', $month)
            ->orderBy('iet.month', 'DESC')
            ->getQuery()
            ->getResult();
    }

    //    public function findOneBySomeField($value): ?IeTaught
    //    {
    //        return $this->createQueryBuilder('i')
    //            ->andWhere('i.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
