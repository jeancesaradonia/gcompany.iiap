<?php

namespace App\Repository;

use App\Entity\IeStartingCourse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<IeStartingCourse>
 *
 * @method IeStartingCourse|null find($id, $lockMode = null, $lockVersion = null)
 * @method IeStartingCourse|null findOneBy(array $criteria, array $orderBy = null)
 * @method IeStartingCourse[]    findAll()
 * @method IeStartingCourse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IeStartingCourseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IeStartingCourse::class);
    }

    public function add(IeStartingCourse $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(IeStartingCourse $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return IeStartingCourse[] Returns an array of IeStartingCourse objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('i.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?IeStartingCourse
//    {
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
