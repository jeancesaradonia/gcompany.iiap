<?php

namespace App\Repository;

use App\Entity\IeSemester;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<IeSemester>
 *
 * @method IeSemester|null find($id, $lockMode = null, $lockVersion = null)
 * @method IeSemester|null findOneBy(array $criteria, array $orderBy = null)
 * @method IeSemester[]    findAll()
 * @method IeSemester[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IeSemesterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IeSemester::class);
    }

    public function add(IeSemester $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(IeSemester $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }


    public function getSemesterByLevel($level)
    {
        return $this->createQueryBuilder('i')
            ->innerJoin('i.level', 'u', 'WITH', 'u.id = :level')
            ->setParameter('level', $level->getId());
    }

    //    public function findOneBySomeField($value): ?IeSemester
    //    {
    //        return $this->createQueryBuilder('i')
    //            ->andWhere('i.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
