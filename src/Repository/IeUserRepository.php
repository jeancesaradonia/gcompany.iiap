<?php

namespace App\Repository;

use App\Entity\IeStudentInformation;
use App\Entity\IeUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;

/**
 * @extends ServiceEntityRepository<IeUser>
 *
 * @method IeUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method IeUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method IeUser[]    findAll()
 * @method IeUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IeUserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IeUser::class);
    }

    public function add(IeUser $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(IeUser $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        if (!$user instanceof IeUser) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newHashedPassword);

        $this->add($user, true);
    }

    /**
     * @return IeUser[] Returns an array of IeUser objects
     */
    public function getUserByRoleType($name): array
    {
        return $this->createQueryBuilder('ieu')
            ->innerJoin('ieu.ieRoles', 'ier', 'WITH', 'ier.name = :name')
            ->setParameter('name', $name)
            ->andWhere('ieu.isDeleted = 0')
            ->orderBy('ieu.id', 'ASC')
            ->getQuery()
            ->getResult();
    }


    /**
     * @return IeUser[] Returns an array of IeUser objects
     */
    public function getRegistrationRequest(): array
    {
        return $this->createQueryBuilder('ieu')
            ->innerJoin('ieu.ieRoles', 'ier', 'WITH', "ier.name = 'ROLE_STUDENT'")
            ->innerJoin('ieu.studentInformation', 'si', 'WITH', "si.matriculeNumber IS NULL")
            ->andWhere('ieu.isDeleted = 0')
            ->orderBy('ieu.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return IeUser[] Returns an array of IeUser objects
     */
    public function getUserByRoleTypeNonConfirm($name): array
    {
        return $this->createQueryBuilder('ieu')
            ->innerJoin('ieu.ieRoles', 'ier', 'WITH', 'ier.name = :name')
            ->setParameter('name', $name)
            ->andWhere('ieu.isDeleted = 0')
            ->andWhere('ieu.confirmAt IS NULL')
            ->orderBy('ieu.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return IeUser[] Returns an array of IeUser objects
     */
    public function getUserByRoleTypeNonVerified($name): array
    {
        return $this->createQueryBuilder('ieu')
            ->innerJoin('ieu.ieRoles', 'ier', 'WITH', 'ier.name = :name')
            ->setParameter('name', $name)
            ->andWhere('ieu.isDeleted = 0')
            ->andWhere('ieu.isVerified = 0')
            ->orderBy('ieu.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return IeUser[] Returns an array of IeUser objects
     */
    public function getUserByStudentInformation(IeStudentInformation $ieStudentInformation)
    {
        return $this->createQueryBuilder('i')
            ->innerJoin('i.studentInformation', 'si', 'WITH', 'si.id = :studentInformation')
            ->setParameter('studentInformation', $ieStudentInformation->getId())
            ->getQuery()
            ->getOneOrNullResult();
    }

    //    public function findOneBySomeField($value): ?IeUser
    //    {
    //        return $this->createQueryBuilder('i')
    //            ->andWhere('i.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
