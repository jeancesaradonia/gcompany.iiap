<?php

namespace App\Repository;

use App\Entity\IePayment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<IePayment>
 *
 * @method IePayment|null find($id, $lockMode = null, $lockVersion = null)
 * @method IePayment|null findOneBy(array $criteria, array $orderBy = null)
 * @method IePayment[]    findAll()
 * @method IePayment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IePaymentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IePayment::class);
    }

    public function add(IePayment $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(IePayment $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return IePayment[] Returns an array of IePayment objects
     */
    public function getRequestUnReadByType($type): array
    {
        return $this->createQueryBuilder('iep')
            ->andWhere("iep.type = :type AND iep.state = 'unread'")
            ->setParameter('type', $type)
            ->orderBy('iep.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return IePayment[] Returns an array of IePayment objects
     */
    public function getClassificationFilter(IePayment $iePayment): array
    {
        $query = $this->createQueryBuilder('iep');
        if ($iePayment->getValidatedAt()) {
            $query->andWhere("iep.validatedAt = :validatedAt")
                ->setParameter('validatedAt', $iePayment->getValidatedAt());
        }
        if ($iePayment->getReason()) {
            $query->andWhere("iep.reason = :reason")
                ->setParameter('reason', $iePayment->getReason());
        }
        if ($iePayment->getType()) {
            $query->andWhere("iep.type = :type")
                ->setParameter('type', $iePayment->getType());
        }
        if ($iePayment->getState()) {
            $query->andWhere("iep.state = :state")
                ->setParameter('state', $iePayment->getState());
        }
        if ($iePayment->getDecision()) {
            $query->andWhere("iep.decision = :decision")
                ->setParameter('decision', $iePayment->getDecision());
        }
        return $query
            ->orderBy('iep.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    //    public function findOneBySomeField($value): ?IePayment
    //    {
    //        return $this->createQueryBuilder('i')
    //            ->andWhere('i.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
