<?php

namespace App\Repository;

use App\Entity\IeFaculty;
use App\Entity\IeLevel;
use App\Entity\IeStudentInformation;
use App\Entity\IeUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<IeStudentInformation>
 *
 * @method IeStudentInformation|null find($id, $lockMode = null, $lockVersion = null)
 * @method IeStudentInformation|null findOneBy(array $criteria, array $orderBy = null)
 * @method IeStudentInformation[]    findAll()
 * @method IeStudentInformation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IeStudentInformationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IeStudentInformation::class);
    }

    public function add(IeStudentInformation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(IeStudentInformation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return IeStudentInformation[] Returns an array of IeStudentInformation objects
     */
    public function getMatriculeNumberByFaculty(IeFaculty $ieFaculty): array
    {
        return $this->createQueryBuilder('si')
            ->innerJoin('si.faculty', 'f', 'WITH', 'f = :faculty')
            ->innerJoin('si.users', 'u')
            ->andWhere('u.isDeleted = false AND u.confirmAt IS NOT NULL')
            ->andWhere('si.matriculeNumber IS NOT NULL')
            ->setParameter('faculty', $ieFaculty)
            ->orderBy('si.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return IeStudentInformation[] Returns an array of IeStudentInformation objects
     */
    public function getNumberStudentOfFaculty(IeFaculty $ieFaculty): array
    {
        return $this->createQueryBuilder('si')
            ->innerJoin('si.faculty', 'f', 'WITH', 'f = :faculty')
            ->innerJoin('si.users', 'u')
            ->andWhere('u.isDeleted = false AND u.confirmAt IS NOT NULL')
            ->andWhere('si.matriculeNumber IS NOT NULL')
            ->setParameter('faculty', $ieFaculty)
            ->orderBy('si.id', 'ASC')
            ->getQuery()
            ->getResult();
    }


    /**
     * @return IeStudentInformation[] Returns an array of IeStudentInformation objects
     */
    public function getStudentByFormationTypeOfFaculty(IeFaculty $ieFaculty, string $formationType): array
    {
        return $this->createQueryBuilder('si')
            ->innerJoin('si.faculty', 'f', 'WITH', 'f = :faculty')
            ->innerJoin('si.users', 'u')
            ->andWhere('u.isDeleted = false AND u.confirmAt IS NOT NULL')
            ->andWhere('si.matriculeNumber IS NOT NULL')
            ->setParameter('faculty', $ieFaculty)
            ->andWhere('si.formationType = :formationType')
            ->setParameter('formationType', $formationType)
            ->orderBy('si.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return IeStudentInformation[] Returns an array of IeStudentInformation objects
     */
    public function getStudentMadagascarOfFaculty(IeFaculty $ieFaculty): array
    {
        return $this->createQueryBuilder('si')
            ->innerJoin('si.faculty', 'f', 'WITH', 'f = :faculty')
            ->innerJoin('si.users', 'u')
            ->andWhere('u.isDeleted = false AND u.confirmAt IS NOT NULL')
            ->andWhere('si.matriculeNumber IS NOT NULL')
            ->setParameter('faculty', $ieFaculty)
            ->andWhere('si.nativeCountry = :nat')
            ->setParameter('nat', 'MG')
            ->orderBy('si.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return IeStudentInformation[] Returns an array of IeStudentInformation objects
     */
    public function getStudentAbroadOfFaculty(IeFaculty $ieFaculty): array
    {
        return $this->createQueryBuilder('si')
            ->innerJoin('si.faculty', 'f', 'WITH', 'f = :faculty')
            ->innerJoin('si.users', 'u')
            ->andWhere('u.isDeleted = false AND u.confirmAt IS NOT NULL')
            ->andWhere('si.matriculeNumber IS NOT NULL')
            ->setParameter('faculty', $ieFaculty)
            ->andWhere('si.nativeCountry != :nat')
            ->setParameter('nat', 'MG')
            ->orderBy('si.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return IeStudentInformation[] Returns an array of IeStudentInformation objects
     */
    public function getStudentByLevelOfFaculty(IeFaculty $ieFaculty, string $level): array
    {
        return $this->createQueryBuilder('si')
            ->innerJoin('si.faculty', 'f', 'WITH', 'f = :faculty')
            ->innerJoin('si.level', 'l', 'WITH', 'l.name = :level')
            ->innerJoin('si.users', 'u')
            ->andWhere('u.isDeleted = false AND u.confirmAt IS NOT NULL')
            ->andWhere('si.matriculeNumber IS NOT NULL')
            ->setParameter('faculty', $ieFaculty)
            ->setParameter('level', $level)
            ->orderBy('si.id', 'ASC')
            ->getQuery()
            ->getResult();
    }


    public function getInformationByUser(IeUser $ieUser): ?IeStudentInformation
    {
        return $this->createQueryBuilder('i')
            ->innerJoin('i.users', 'u', 'WITH', 'u.id = :user')
            ->setParameter('user', $ieUser->getId())
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @return IeStudentInformation[] Returns an array of IeStudentInformation objects
     */
    public function getRegistratedStudentByFaculty(IeFaculty $ieFaculty): array
    {
        return $this->createQueryBuilder('si')
            ->innerJoin('si.faculty', 'f', 'WITH', 'f = :faculty')
            ->innerJoin('si.users', 'u')
            ->setParameter('faculty', $ieFaculty)
            ->andWhere('u.isDeleted = false AND u.confirmAt IS NOT NULL')
            ->andWhere('si.matriculeNumber IS NOT NULL')
            ->orderBy('si.id', 'ASC')
            ->getQuery()
            ->getResult();
    }


    /**
     * @return IeStudentInformation[] Returns an array of IeStudentInformation objects
     */
    public function getFilterStudentInformation(IeStudentInformation $ieStudentInformation): array
    {
        // dd($ieStudentInformation);
        $queryB = $this->createQueryBuilder('si')
            ->innerJoin('si.users', 'u')
            ->innerJoin('si.faculty', 'f', 'WITH', 'f = :faculty')
            ->setParameter('faculty', $ieStudentInformation->getFaculty());

        if ($ieStudentInformation->getType()) $queryB->andWhere('si.formationType = :type')->setParameter('type', $ieStudentInformation->getType());
        if ($ieStudentInformation->getLevel()) $queryB->innerJoin('si.level', 'l', 'WITH', 'l = :level')->setParameter('level', $ieStudentInformation->getLevel());
        if ($ieStudentInformation->getSemester()) $queryB->innerJoin('si.semester', 's', 'WITH', 's = :semester')->setParameter('semester', $ieStudentInformation->getSemester());
        if ($ieStudentInformation->getVague()) $queryB->innerJoin('si.vague', 'v', 'WITH', 'v = :vague')->setParameter('vague', $ieStudentInformation->getVague());

        return $queryB
            ->andWhere('u.isDeleted = false AND u.confirmAt IS NOT NULL')
            ->andWhere('si.matriculeNumber IS NOT NULL')
            ->orderBy('si.id', 'ASC')
            ->getQuery()
            ->getResult();
    }


    /**
     * @return IeStudentInformation[] Returns an array of IeStudentInformation objects
     */
    public function getPaymentStatusStudent(IeStudentInformation $ieStudentInformation): array
    {
        // dd($ieStudentInformation);
        $queryB = $this->createQueryBuilder('si')
            ->innerJoin('si.users', 'u');

        if ($ieStudentInformation->getType()) $queryB->andWhere('si.formationType = :type')->setParameter('type', $ieStudentInformation->getType());
        if ($ieStudentInformation->getLevel()) $queryB->innerJoin('si.level', 'l', 'WITH', 'l = :level')->setParameter('level', $ieStudentInformation->getLevel());
        if ($ieStudentInformation->getFaculty()) $queryB->innerJoin('si.faculty', 'f', 'WITH', 'f = :faculty')->setParameter('faculty', $ieStudentInformation->getFaculty());
        if ($ieStudentInformation->getSemester()) $queryB->innerJoin('si.semester', 's', 'WITH', 's = :semester')->setParameter('semester', $ieStudentInformation->getSemester());
        if ($ieStudentInformation->getVague()) $queryB->innerJoin('si.vague', 'v', 'WITH', 'v = :vague')->setParameter('vague', $ieStudentInformation->getVague());

        return $queryB
            ->andWhere('u.isDeleted = false AND u.confirmAt IS NOT NULL')
            ->andWhere('si.matriculeNumber IS NOT NULL')
            ->orderBy('si.id', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
