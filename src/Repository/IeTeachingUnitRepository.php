<?php

namespace App\Repository;

use App\Entity\IeTeachingUnit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<IeTeachingUnit>
 *
 * @method IeTeachingUnit|null find($id, $lockMode = null, $lockVersion = null)
 * @method IeTeachingUnit|null findOneBy(array $criteria, array $orderBy = null)
 * @method IeTeachingUnit[]    findAll()
 * @method IeTeachingUnit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IeTeachingUnitRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IeTeachingUnit::class);
    }

    public function add(IeTeachingUnit $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(IeTeachingUnit $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return IeTeachingUnit[] Returns an array of IeTeachingUnit objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('i.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?IeTeachingUnit
//    {
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
