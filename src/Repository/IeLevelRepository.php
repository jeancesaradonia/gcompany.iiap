<?php

namespace App\Repository;

use App\Entity\IeLevel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<IeLevel>
 *
 * @method IeLevel|null find($id, $lockMode = null, $lockVersion = null)
 * @method IeLevel|null findOneBy(array $criteria, array $orderBy = null)
 * @method IeLevel[]    findAll()
 * @method IeLevel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IeLevelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IeLevel::class);
    }

    public function add(IeLevel $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(IeLevel $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return IeLevel[] Returns an array of IeLevel objects
     */
    public function getLevelForRegistrationFront()
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.name != :name')
            ->setParameter('name', "Licence 3")
            ->orderBy('i.name', 'ASC');
    }

    //    public function findOneBySomeField($value): ?IeLevel
    //    {
    //        return $this->createQueryBuilder('i')
    //            ->andWhere('i.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
