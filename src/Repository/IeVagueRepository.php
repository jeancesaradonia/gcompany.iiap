<?php

namespace App\Repository;

use App\Entity\IeVague;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<IeVague>
 *
 * @method IeVague|null find($id, $lockMode = null, $lockVersion = null)
 * @method IeVague|null findOneBy(array $criteria, array $orderBy = null)
 * @method IeVague[]    findAll()
 * @method IeVague[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IeVagueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IeVague::class);
    }

    public function add(IeVague $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(IeVague $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }


    /**
     * @return IeVague[] Returns an array of IeVague objects
     */
    public function getStartedVague(): array
    {
        return $this->createQueryBuilder('v')
            ->orderBy('v.id', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
