<?php

namespace App\Entity;

use App\Repository\IeResultsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=IeResultsRepository::class)
 */
class IeResults
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $response;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $file;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $note;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $beginAt;

    /**
     * @ORM\ManyToOne(targetEntity=IeStudentInformation::class, inversedBy="results")
     * @ORM\JoinColumn(nullable=false)
     */
    private $student;

    /**
     * @ORM\ManyToOne(targetEntity=IeExamSession::class, inversedBy="results")
     * @ORM\JoinColumn(nullable=false)
     */
    private $sessionExam;

    /**
     * @ORM\ManyToOne(targetEntity=IeCourse::class, inversedBy="results")
     * @ORM\JoinColumn(nullable=false)
     */
    private $course;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getResponse(): ?string
    {
        return $this->response;
    }

    public function setResponse(?string $response): self
    {
        $this->response = $response;

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getNote(): ?float
    {
        return $this->note;
    }

    public function setNote(?float $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getBeginAt(): ?\DateTimeInterface
    {
        return $this->beginAt;
    }

    public function setBeginAt(?\DateTimeInterface $beginAt): self
    {
        $this->beginAt = $beginAt;

        return $this;
    }

    public function getStudent(): ?IeStudentInformation
    {
        return $this->student;
    }

    public function setStudent(?IeStudentInformation $student): self
    {
        $this->student = $student;

        return $this;
    }

    public function getSessionExam(): ?IeExamSession
    {
        return $this->sessionExam;
    }

    public function setSessionExam(?IeExamSession $sessionExam): self
    {
        $this->sessionExam = $sessionExam;

        return $this;
    }

    public function getCourse(): ?IeCourse
    {
        return $this->course;
    }

    public function setCourse(?IeCourse $course): self
    {
        $this->course = $course;

        return $this;
    }
}
