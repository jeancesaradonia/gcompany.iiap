<?php

namespace App\Entity;

use App\Repository\IeRoleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=IeRoleRepository::class)
 * @ORM\Table(name="ie_roles")
 */
class IeRole
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity=IeUser::class, mappedBy="ieRoles")
     */
    private $ieUsers;



    public function __construct()
    {
        $this->ieUsers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, IeUser>
     */
    public function getIeUsers(): Collection
    {
        return $this->ieUsers;
    }

    public function addIeUser(IeUser $ieUser): self
    {
        if (!$this->ieUsers->contains($ieUser)) {
            $this->ieUsers[] = $ieUser;
            $ieUser->addIeRole($this);
        }

        return $this;
    }

    public function removeIeUser(IeUser $ieUser): self
    {
        if ($this->ieUsers->removeElement($ieUser)) {
            $ieUser->removeIeRole($this);
        }

        return $this;
    }
}
