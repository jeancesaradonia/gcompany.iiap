<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\Timestampable;
use App\Repository\IeCourseContentRepository;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=IeCourseContentRepository::class)
 * @Vich\Uploadable
 * @ORM\Table(name="ie_course_contents")
 * @ORM\HasLifecycleCallbacks
 */
class IeCourseContent
{
    use Timestampable;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $contentType;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $file;

    /**
     * @ORM\ManyToOne(targetEntity=IeCourse::class, inversedBy="courseContents")
     * @ORM\JoinColumn(nullable=false)
     */
    private $course;

    /**
     * @Vich\UploadableField(mapping="course_content_file", fileNameProperty="file")
     * @Assert\File(
     *     maxSize = "100M",
     * )
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $courseType;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContentType(): ?string
    {
        return $this->contentType;
    }

    public function setContentType(string $contentType): self
    {
        $this->contentType = $contentType;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getCourse(): ?IeCourse
    {
        return $this->course;
    }

    public function setCourse(?IeCourse $course): self
    {
        $this->course = $course;

        return $this;
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;
        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function getCourseType(): ?string
    {
        return $this->courseType;
    }

    public function setCourseType(string $courseType): self
    {
        $this->courseType = $courseType;

        return $this;
    }
}
