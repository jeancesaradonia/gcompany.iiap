<?php

namespace App\Entity;

use App\Repository\IeTeachingUnitRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=IeTeachingUnitRepository::class)
 * @ORM\Table(name="ie_teaching_units")
 */
class IeTeachingUnit
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=IeCourse::class, mappedBy="teachingUnit")
     */
    private $courses;

    public function __construct()
    {
        $this->courses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, IeCourse>
     */
    public function getCourses(): Collection
    {
        return $this->courses;
    }

    public function addCourse(IeCourse $course): self
    {
        if (!$this->courses->contains($course)) {
            $this->courses[] = $course;
            $course->setTeachingUnit($this);
        }

        return $this;
    }

    public function removeCourse(IeCourse $course): self
    {
        if ($this->courses->removeElement($course)) {
            // set the owning side to null (unless already changed)
            if ($course->getTeachingUnit() === $this) {
                $course->setTeachingUnit(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }
}
