<?php

namespace App\Entity;

use App\Repository\IeTaughtRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=IeTaughtRepository::class)
 * @ORM\Table(name="ie_taughs")
 */
class IeTaught
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $month;

    /**
     * @ORM\ManyToOne(targetEntity=IeFaculty::class, inversedBy="taughts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $faculty;

    /**
     * @ORM\ManyToOne(targetEntity=IeSemester::class, inversedBy="taughts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $semester;

    /**
     * @ORM\ManyToOne(targetEntity=IeTeacherInformation::class, inversedBy="taughts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $teacherInformation;

    /**
     * @ORM\ManyToOne(targetEntity=IeCourse::class, inversedBy="taughts")
     */
    private $course;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMonth(): ?int
    {
        return $this->month;
    }

    public function setMonth(int $month): self
    {
        $this->month = $month;

        return $this;
    }

    public function getFaculty(): ?IeFaculty
    {
        return $this->faculty;
    }

    public function setFaculty(?IeFaculty $faculty): self
    {
        $this->faculty = $faculty;

        return $this;
    }

    public function getSemester(): ?IeSemester
    {
        return $this->semester;
    }

    public function setSemester(?IeSemester $semester): self
    {
        $this->semester = $semester;

        return $this;
    }

    public function getTeacherInformation(): ?IeTeacherInformation
    {
        return $this->teacherInformation;
    }

    public function setTeacherInformation(?IeTeacherInformation $teacherInformation): self
    {
        $this->teacherInformation = $teacherInformation;

        return $this;
    }

    public function getCourse(): ?IeCourse
    {
        return $this->course;
    }

    public function setCourse(?IeCourse $course): self
    {
        $this->course = $course;

        return $this;
    }
}
