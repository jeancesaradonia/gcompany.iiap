<?php

namespace App\Entity;

use App\Repository\IeCourseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=IeCourseRepository::class)
 * @Vich\Uploadable
 */
class IeCourse
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $credit;

    /**
     * @ORM\ManyToOne(targetEntity=IeTeachingUnit::class, inversedBy="courses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $teachingUnit;

    /**
     * @ORM\OneToMany(targetEntity=IeTaught::class, mappedBy="course")
     */
    private $taughts;

    /**
     * @ORM\OneToMany(targetEntity=IeCourseContent::class, mappedBy="course")
     */
    private $courseContents;

    /**
     * @Vich\UploadableField(mapping="ue_images", fileNameProperty="image")
     * @Assert\NotBlank(message="Ce cours doit avoir une image.")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity=IeExam::class, mappedBy="course")
     */
    private $exams;

    /**
     * @ORM\OneToMany(targetEntity=IeResults::class, mappedBy="course")
     */
    private $results;

    public function __construct()
    {
        $this->taughts = new ArrayCollection();
        $this->courseContents = new ArrayCollection();
        $this->exams = new ArrayCollection();
        $this->results = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getCredit(): ?string
    {
        return $this->credit;
    }

    public function setCredit(?string $credit): self
    {
        $this->credit = $credit;

        return $this;
    }

    public function getTeachingUnit(): ?IeTeachingUnit
    {
        return $this->teachingUnit;
    }

    public function setTeachingUnit(?IeTeachingUnit $teachingUnit): self
    {
        $this->teachingUnit = $teachingUnit;

        return $this;
    }

    /**
     * @return Collection<int, IeTaught>
     */
    public function getTaughts(): Collection
    {
        return $this->taughts;
    }

    public function addTaught(IeTaught $taught): self
    {
        if (!$this->taughts->contains($taught)) {
            $this->taughts[] = $taught;
            $taught->setCourse($this);
        }

        return $this;
    }

    public function removeTaught(IeTaught $taught): self
    {
        if ($this->taughts->removeElement($taught)) {
            // set the owning side to null (unless already changed)
            if ($taught->getCourse() === $this) {
                $taught->setCourse(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, IeCourseContent>
     */
    public function getCourseContents(): Collection
    {
        return $this->courseContents;
    }

    public function addCourseContent(IeCourseContent $courseContent): self
    {
        if (!$this->courseContents->contains($courseContent)) {
            $this->courseContents[] = $courseContent;
            $courseContent->setCourse($this);
        }

        return $this;
    }

    public function removeCourseContent(IeCourseContent $courseContent): self
    {
        if ($this->courseContents->removeElement($courseContent)) {
            // set the owning side to null (unless already changed)
            if ($courseContent->getCourse() === $this) {
                $courseContent->setCourse(null);
            }
        }

        return $this;
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function __toString()
    {
        return $this->title;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection<int, IeExam>
     */
    public function getExams(): Collection
    {
        return $this->exams;
    }

    public function addExam(IeExam $exam): self
    {
        if (!$this->exams->contains($exam)) {
            $this->exams[] = $exam;
            $exam->setCourse($this);
        }

        return $this;
    }

    public function removeExam(IeExam $exam): self
    {
        if ($this->exams->removeElement($exam)) {
            // set the owning side to null (unless already changed)
            if ($exam->getCourse() === $this) {
                $exam->setCourse(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, IeResults>
     */
    public function getResults(): Collection
    {
        return $this->results;
    }

    public function addResult(IeResults $result): self
    {
        if (!$this->results->contains($result)) {
            $this->results[] = $result;
            $result->setCourse($this);
        }

        return $this;
    }

    public function removeResult(IeResults $result): self
    {
        if ($this->results->removeElement($result)) {
            // set the owning side to null (unless already changed)
            if ($result->getCourse() === $this) {
                $result->setCourse(null);
            }
        }

        return $this;
    }
}
