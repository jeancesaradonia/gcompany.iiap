<?php

namespace App\Entity;

use App\Repository\IePageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=IePageRepository::class)
 * @ORM\Table(name="ie_pages")
 */
class IePage
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255,  nullable=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $view;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\ManyToMany(targetEntity=IePage::class, inversedBy="iePages")
     */
    private $pages;

    /**
     * @ORM\ManyToMany(targetEntity=IePage::class, mappedBy="pages")
     */
    private $iePages;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isDeleted;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $linkText;

    /**
     * @ORM\Column(type="integer")
     */
    private $menuOrder;



    public function __construct()
    {
        $this->pages = new ArrayCollection();
        $this->iePages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getView(): ?string
    {
        return $this->view;
    }

    public function setView(string $view): self
    {
        $this->view = $view;

        return $this;
    }

    public function isIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getPages(): Collection
    {
        return $this->pages;
    }

    public function addPage(self $page): self
    {
        if (!$this->pages->contains($page)) {
            $this->pages[] = $page;
        }

        return $this;
    }

    public function removePage(self $page): self
    {
        $this->pages->removeElement($page);

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getIePages(): Collection
    {
        return $this->iePages;
    }

    public function addIePage(self $iePage): self
    {
        if (!$this->iePages->contains($iePage)) {
            $this->iePages[] = $iePage;
            $iePage->addPage($this);
        }

        return $this;
    }

    public function removeIePage(self $iePage): self
    {
        if ($this->iePages->removeElement($iePage)) {
            $iePage->removePage($this);
        }

        return $this;
    }

    public function isIsDeleted(): bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): self
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    public function __toString(): string
    {
        return $this->getTitle();
    }

    public function getLinkText(): ?string
    {
        return $this->linkText;
    }

    public function setLinkText(string $linkText): self
    {
        $this->linkText = $linkText;

        return $this;
    }

    public function getMenuOrder(): ?int
    {
        return $this->menuOrder;
    }

    public function setMenuOrder(int $menuOrder): self
    {
        $this->menuOrder = $menuOrder;

        return $this;
    }
}
