<?php

namespace App\Entity;

use App\Repository\IeSemesterRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=IeSemesterRepository::class)
 * @ORM\Table(name="ie_semesters")
 */
class IeSemester
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $number;

    /**
     * @ORM\Column(type="integer")
     */
    private $year;

    /**
     * @ORM\ManyToOne(targetEntity=IeLevel::class, inversedBy="semesters")
     * @ORM\JoinColumn(nullable=false)
     */
    private $level;

    /**
     * @ORM\OneToMany(targetEntity=IeStudentInformation::class, mappedBy="semester")
     */
    private $studentInformation;

    /**
     * @ORM\OneToMany(targetEntity=IeTaught::class, mappedBy="semester")
     */
    private $taughts;

    /**
     * @ORM\OneToMany(targetEntity=IeTimetable::class, mappedBy="semester")
     */
    private $timetables;


    public function __construct()
    {
        $this->studentInformation = new ArrayCollection();
        $this->taughts = new ArrayCollection();
        $this->timetables = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getLevel(): ?IeLevel
    {
        return $this->level;
    }

    public function setLevel(?IeLevel $level): self
    {
        $this->level = $level;

        return $this;
    }

    /**
     * @return Collection<int, IeStudentInformation>
     */
    public function getStudentInformation(): Collection
    {
        return $this->studentInformation;
    }

    public function addStudentInformation(IeStudentInformation $studentInformation): self
    {
        if (!$this->studentInformation->contains($studentInformation)) {
            $this->studentInformation[] = $studentInformation;
            $studentInformation->setSemester($this);
        }

        return $this;
    }

    public function removeStudentInformation(IeStudentInformation $studentInformation): self
    {
        if ($this->studentInformation->removeElement($studentInformation)) {
            // set the owning side to null (unless already changed)
            if ($studentInformation->getSemester() === $this) {
                $studentInformation->setSemester(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return 'Semetre ' . $this->getNumber();
    }

    /**
     * @return Collection<int, IeTaught>
     */
    public function getTaughts(): Collection
    {
        return $this->taughts;
    }

    public function addTaught(IeTaught $taught): self
    {
        if (!$this->taughts->contains($taught)) {
            $this->taughts[] = $taught;
            $taught->setSemester($this);
        }

        return $this;
    }

    public function removeTaught(IeTaught $taught): self
    {
        if ($this->taughts->removeElement($taught)) {
            // set the owning side to null (unless already changed)
            if ($taught->getSemester() === $this) {
                $taught->setSemester(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, IeTimetable>
     */
    public function getTimetables(): Collection
    {
        return $this->timetables;
    }

    public function addTimetable(IeTimetable $timetable): self
    {
        if (!$this->timetables->contains($timetable)) {
            $this->timetables[] = $timetable;
            $timetable->setSemester($this);
        }

        return $this;
    }

    public function removeTimetable(IeTimetable $timetable): self
    {
        if ($this->timetables->removeElement($timetable)) {
            // set the owning side to null (unless already changed)
            if ($timetable->getSemester() === $this) {
                $timetable->setSemester(null);
            }
        }

        return $this;
    }
}
