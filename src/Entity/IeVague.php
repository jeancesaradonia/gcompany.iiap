<?php

namespace App\Entity;

use App\Repository\IeVagueRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=IeVagueRepository::class)
 * @ORM\Table(name="ie_vagues")
 */
class IeVague
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $vNumber;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $vDescription;

    /**
     * @ORM\OneToMany(targetEntity=IeStudentInformation::class, mappedBy="vague")
     */
    private $studentInformation;


    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private $startAt;

    /**
     * @ORM\OneToMany(targetEntity=IeTimetable::class, mappedBy="vague")
     */
    private $timetables;

    public function __construct()
    {
        $this->studentInformation = new ArrayCollection();
        $this->timetables = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVNumber(): ?int
    {
        return $this->vNumber;
    }

    public function setVNumber(int $vNumber): self
    {
        $this->vNumber = $vNumber;

        return $this;
    }

    public function getVDescription(): ?string
    {
        return $this->vDescription;
    }

    public function setVDescription(?string $vDescription): self
    {
        $this->vDescription = $vDescription;

        return $this;
    }

    /**
     * @return Collection<int, IeStudentInformation>
     */
    public function getStudentInformation(): Collection
    {
        return $this->studentInformation;
    }

    public function addStudentInformation(IeStudentInformation $studentInformation): self
    {
        if (!$this->studentInformation->contains($studentInformation)) {
            $this->studentInformation[] = $studentInformation;
            $studentInformation->setVague($this);
        }

        return $this;
    }

    public function removeStudentInformation(IeStudentInformation $studentInformation): self
    {
        if ($this->studentInformation->removeElement($studentInformation)) {
            // set the owning side to null (unless already changed)
            if ($studentInformation->getVague() === $this) {
                $studentInformation->setVague(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return 'Vague ' . $this->getVNumber();
    }

    public function getStartAt(): ?\DateTimeImmutable
    {
        return $this->startAt;
    }

    public function setStartAt(?\DateTimeImmutable $startAt): self
    {
        $this->startAt = $startAt;

        return $this;
    }

    /**
     * @return Collection<int, IeTimetable>
     */
    public function getTimetables(): Collection
    {
        return $this->timetables;
    }

    public function addTimetable(IeTimetable $timetable): self
    {
        if (!$this->timetables->contains($timetable)) {
            $this->timetables[] = $timetable;
            $timetable->setVague($this);
        }

        return $this;
    }

    public function removeTimetable(IeTimetable $timetable): self
    {
        if ($this->timetables->removeElement($timetable)) {
            // set the owning side to null (unless already changed)
            if ($timetable->getVague() === $this) {
                $timetable->setVague(null);
            }
        }

        return $this;
    }
}
