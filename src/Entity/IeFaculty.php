<?php

namespace App\Entity;

use App\Repository\IeFacultyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=IeFacultyRepository::class)
 * @ORM\Table(name="ie_faculties")
 */
class IeFaculty
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $acronym;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=IeStudentInformation::class, mappedBy="faculty")
     */
    private $studentInformations;

    /**
     * @ORM\OneToMany(targetEntity=IeTaught::class, mappedBy="faculty")
     */
    private $taughts;

    /**
     * @ORM\OneToMany(targetEntity=IeTimetable::class, mappedBy="faculty")
     */
    private $timetables;

    public function __construct()
    {
        $this->studentInformations = new ArrayCollection();
        $this->taughts = new ArrayCollection();
        $this->timetables = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAcronym(): ?string
    {
        return $this->acronym;
    }

    public function setAcronym(string $acronym): self
    {
        $this->acronym = $acronym;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, IeStudentInformation>
     */
    public function getStudentInformations(): Collection
    {
        return $this->studentInformations;
    }

    public function addStudentInformations(IeStudentInformation $student): self
    {
        if (!$this->studentInformations->contains($student)) {
            $this->studentInformations[] = $student;
            $student->setFaculty($this);
        }

        return $this;
    }

    public function removeStudentInformations(IeStudentInformation $student): self
    {
        if ($this->studentInformations->removeElement($student)) {
            // set the owning side to null (unless already changed)
            if ($student->getFaculty() === $this) {
                $student->setFaculty(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return Collection<int, IeTaught>
     */
    public function getTaughts(): Collection
    {
        return $this->taughts;
    }

    public function addTaught(IeTaught $taught): self
    {
        if (!$this->taughts->contains($taught)) {
            $this->taughts[] = $taught;
            $taught->setFaculty($this);
        }

        return $this;
    }

    public function removeTaught(IeTaught $taught): self
    {
        if ($this->taughts->removeElement($taught)) {
            // set the owning side to null (unless already changed)
            if ($taught->getFaculty() === $this) {
                $taught->setFaculty(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, IeTimetable>
     */
    public function getTimetables(): Collection
    {
        return $this->timetables;
    }

    public function addTimetable(IeTimetable $timetable): self
    {
        if (!$this->timetables->contains($timetable)) {
            $this->timetables[] = $timetable;
            $timetable->setFaculty($this);
        }

        return $this;
    }

    public function removeTimetable(IeTimetable $timetable): self
    {
        if ($this->timetables->removeElement($timetable)) {
            // set the owning side to null (unless already changed)
            if ($timetable->getFaculty() === $this) {
                $timetable->setFaculty(null);
            }
        }

        return $this;
    }
}
