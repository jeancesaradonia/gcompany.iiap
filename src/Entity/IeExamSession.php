<?php

namespace App\Entity;

use App\Repository\IeExamSessionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=IeExamSessionRepository::class)
 * @ORM\Table(name="ie_exam_sessions")
 */
class IeExamSession
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity=IeExam::class, mappedBy="session")
     */
    private $exams;

    /**
     * @ORM\OneToMany(targetEntity=IeResults::class, mappedBy="sessionExam")
     */
    private $results;

    public function __construct()
    {
        $this->exams = new ArrayCollection();
        $this->results = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection<int, IeExam>
     */
    public function getExams(): Collection
    {
        return $this->exams;
    }

    public function addExam(IeExam $exam): self
    {
        if (!$this->exams->contains($exam)) {
            $this->exams[] = $exam;
            $exam->setSession($this);
        }

        return $this;
    }

    public function removeExam(IeExam $exam): self
    {
        if ($this->exams->removeElement($exam)) {
            // set the owning side to null (unless already changed)
            if ($exam->getSession() === $this) {
                $exam->setSession(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->type;
    }

    /**
     * @return Collection<int, IeResults>
     */
    public function getResults(): Collection
    {
        return $this->results;
    }

    public function addResult(IeResults $result): self
    {
        if (!$this->results->contains($result)) {
            $this->results[] = $result;
            $result->setSessionExam($this);
        }

        return $this;
    }

    public function removeResult(IeResults $result): self
    {
        if ($this->results->removeElement($result)) {
            // set the owning side to null (unless already changed)
            if ($result->getSessionExam() === $this) {
                $result->setSessionExam(null);
            }
        }

        return $this;
    }
}
