<?php

namespace App\DataFixtures;

use App\Entity\IeDegree;
use App\Entity\IeFaculty;
use App\Entity\IeLevel;
use App\Entity\IeRole;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {

        // ROLES

        $roleTypes = [
            "ROLE_STUDENT" => "Étudiant",
            "ROLE_ADMIN" => "Administrateur",
            "ROLE_TEACHER" => "Proffesseur",
            "ROLE_SCHOOL_MANAGER" => "Géstionnaire d'écolage"
        ];

        $roles = [];
        foreach ($roleTypes as $name => $description) {
            $ieRoles = new IeRole();
            $ieRoles->setName($name)->setDescription($description);
            $roles[] = $ieRoles;
            $manager->persist($ieRoles);
        }

        // DIPLOME

        $degreeTypes = [
            "LICENCE" => "Diplome de licence",
            "MASTER" => "diplome de master"
        ];

        $degrees = [];
        foreach ($degreeTypes as $name => $description) {
            $ieDegree = new IeDegree();
            $ieDegree->setName($name)->setDescription($description);
            $degrees[] = $ieDegree;
            $manager->persist($ieDegree);
        }

        // NIVEAU

        $levelTypes = [
            "Licence 1" => "Licence 1",
            "Licence 2" => "Licence 2",
            "Licence 3" => "Licence 3",
            "Master 1" => "Master 1",
            "Master 2" => "Master 2"
        ];

        $levels = [];
        $counterDegree = 0;
        foreach ($levelTypes as $name => $description) {
            $ieLevel = new IeLevel();
            $ieLevel->setName($name)->setDescription($description);

            if ($counterDegree < 3) {
                $ieLevel->setDegree($degrees[0]);
            } else {
                $ieLevel->setDegree($degrees[1]);
            }
            $counterDegree++;
            $levels[] = $ieLevel;
            $manager->persist($ieLevel);
        }


        // FILIÈRE

        $facultyTypes = [
            'DAPT' => "Droit, Administration Publique et Territoriale",
            'GPDD' => "Gestion de projet et Développement Durable",
            'MFP' => "Marchés et Finances Publiques",
            'OIHD' => "Organisations Internationales, Humanitaires et Diplomatie",
        ];

        $faculties = [];
        foreach ($facultyTypes as $acronym => $name) {
            $ieFaculty = new IeFaculty();
            $ieFaculty->setName($name)->setAcronym($acronym)->setDescription($name . '.' . $name);
            $faculties[] = $ieFaculty;
            $manager->persist($ieFaculty);
        }

        $manager->flush();
    }
}
