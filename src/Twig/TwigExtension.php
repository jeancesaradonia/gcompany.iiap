<?php

namespace App\Twig;

use App\Entity\IeLevel;
use App\Entity\IeTaught;
use App\Entity\IeUser;
use App\Repository\IeTaughtRepository;
use App\Repository\IeStudentInformationRepository;
use App\Repository\IeUserRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class TwigExtension extends AbstractExtension
{

    private IeUserRepository $ieUserRepository;
    private IeTaughtRepository $IeTaughtRepository;
    private IeStudentInformationRepository $ieStudentInformationRepository;
    public function __construct(
        IeUserRepository $ieUserRepository,
        IeTaughtRepository $IeTaughtRepository,
        IeStudentInformationRepository $ieStudentInformationRepository
    ) {
        $this->ieUserRepository = $ieUserRepository;
        $this->IeTaughtRepository = $IeTaughtRepository;
        $this->ieStudentInformationRepository = $ieStudentInformationRepository;
    }

    public function getFunctions(): array
    {
        return [
            // conversion pour le role d'utilisateur (FR)
            new TwigFunction('convert', [$this, 'convert']),

            // icon pour le role
            new TwigFunction('renderIcon', [$this, 'renderIcon']),

            // retour le modalité d'inscription (FR)
            new TwigFunction('getFormationType', [$this, 'getFormationType']),

            // retourne le type (Étudiant, travailleur)
            new TwigFunction('getType', [$this, 'getType']),

            // retourn le sexe (FR)
            new TwigFunction('getSexe', [$this, 'getSexe']),

            // retourn l'utilisateur lié par le studentInformation            
            new TwigFunction('getUser', [$this, 'getUser']),

            // retourne le max number month selon niveau
            new TwigFunction('getMaxNumberMonthlyFeesForLevel', [$this, 'getMaxNumberMonthlyFeesForLevel']),

            // retourne HTML progressBar selon mois 
            new TwigFunction('getProgressbarMonthlyFees', [$this, 'getProgressbarMonthlyFees']),

            // retourne la raison de paiment (FR)
            new TwigFunction('getReason', [$this, 'getReason']),

            // retourne le nombre d'étudiants selon filière par formationType
            new TwigFunction('getNumberFormationTypeByFaculty', [$this, 'getNumberFormationTypeByFaculty']),

            // retourne le nombre d'étudiants selon filière par native
            new TwigFunction('getNumberNativeCountryByFaculty', [$this, 'getNumberNativeCountryByFaculty']),

            // retourne le nombre d'étudiants selon filière par level
            new TwigFunction('getNumberLevelByFaculty', [$this, 'getNumberLevelByFaculty']),

            // retourne le nombre d'étudiants selon filière
            new TwigFunction('getNumberStudentByFaculty', [$this, 'getNumberStudentByFaculty']),



            // recupère les course selon les formations
            new TwigFunction('getCourse', [$this, 'getCourse']),

        ];
    }

    // mamadika ny role en français
    public function convert($value)
    {
        return [
            'ROLE_STUDENT' => 'Étudiant',
            'ROLE_ADMIN' => 'Administrateur',
            'ROLE_TEACHER' => "Proffesseur",
            'ROLE_SCHOOL_MANAGER' => "Géstionnaire d'écolage"
        ][$value];
    }

    // mamadiaka ny type de formation étudiant ho lasa français
    public function getFormationType($formationType)
    {
        return [
            'face-to-face' => 'Formation en présentiel',
            'online' => 'Formation en ligne',
        ][$formationType];
    }

    // 
    public function getType($student)
    {
        return [
            'bachelor.male' => 'Bachelier/ère',
            'bachelor.feminine' => 'Bachelière',
            'worker.male' => 'Travailleur',
            'worker.feminine' => 'Travailleuse',
        ][$student];
    }

    public function getSexe($sexe)
    {
        return [
            'male' => 'Masculin',
            'feminine' => 'Féminin',
        ][$sexe];
    }

    public function getUser($studentInformation)
    {
        return $this->ieUserRepository->getUserByStudentInformation($studentInformation);
    }

    public function getMaxNumberMonthlyFeesForLevel(IeLevel $ieLevel)
    {
        $degree = $ieLevel->getDegree()->getName();
        $maxNumber = 0;
        switch ($degree) {
            case 'LICENCE':
                $maxNumber = 8;
                break;
            case 'MASTER':
                $maxNumber = 10;
                break;

            default:
                $maxNumber = 0;
                break;
        }
        return $maxNumber;
        // return $maxNumber . " (" . $degree . ")";
    }

    public function getProgressbarMonthlyFees($numberMonthlyFees, IeLevel $ieLevel)
    {
        $degree = $ieLevel->getDegree()->getName();
        $maxNumber = $this->getMaxNumberMonthlyFeesForLevel($ieLevel);

        $actualPercentage = ($numberMonthlyFees * 100) / $maxNumber;
        $htmlElement = '<div class="progress progress-sm">
                            <div class="progress-bar bg-green" role="progressbar" aria-valuenow="' . $numberMonthlyFees . '" aria-valuemin="0" aria-valuemax="' . $maxNumber . '" style="width: ' . $actualPercentage . '%">
                            </div>
                        </div>
                        <small>
                              ' . $actualPercentage . '% terminé
                          </small>
                        ';
        return $htmlElement;
    }


    public function renderIcon($value)
    {
        return [
            'ROLE_STUDENT' => 'fa-graduation-cap',
            'ROLE_ADMIN' => 'fa-screwdriver',
            'ROLE_TEACHER' => "fa-chalkboard-teacher",
            'ROLE_SCHOOL_MANAGER' => "fa-credit-card"
        ][$value];
    }

    public function getReason($reason)
    {
        return [
            'registration-fee' => 'Frais d\'inscription',
            'monthly-fees' => 'Écolage',
            'examination-fees' => 'Droit d\'examen',
            'defense-fee' => 'Droit de soutenance',
            'certificate-fee' => 'Certificat',
        ][$reason];
    }







    // Extension pour les filière

    public function getNumberFormationTypeByFaculty($faculty)
    {
        $faceToFace = $this->ieStudentInformationRepository->getStudentByFormationTypeOfFaculty($faculty, "face-to-face");
        $online = $this->ieStudentInformationRepository->getStudentByFormationTypeOfFaculty($faculty, "online");

        return [
            'face-to-face' => count($faceToFace),
            'online' => count($online)
        ];
    }

    public function getNumberNativeCountryByFaculty($faculty)
    {
        $madagascar = $this->ieStudentInformationRepository->getStudentMadagascarOfFaculty($faculty);
        $abroad = $this->ieStudentInformationRepository->getStudentAbroadOfFaculty($faculty);
        return [
            'madagascar' => count($madagascar),
            'abroad' => count($abroad)
        ];
    }

    public function getNumberLevelByFaculty($faculty)
    {
        return [
            'L1' => count($this->ieStudentInformationRepository->getStudentByLevelOfFaculty($faculty, "Licence 1")),
            'L2' => count($this->ieStudentInformationRepository->getStudentByLevelOfFaculty($faculty, "Licence 2")),
            'L3' => count($this->ieStudentInformationRepository->getStudentByLevelOfFaculty($faculty, "Licence 3")),
            'M1' => count($this->ieStudentInformationRepository->getStudentByLevelOfFaculty($faculty, "Master 1")),
            'M2' => count($this->ieStudentInformationRepository->getStudentByLevelOfFaculty($faculty, "Master 2")),
        ];
    }

    public function getNumberStudentByFaculty($faculty)
    {
        $students = $this->ieStudentInformationRepository->getNumberStudentOfFaculty($faculty);
        return count($students);
    }


    public function getCourse(IeTaught $ieTaught)
    {
        $courses = $this->IeTaughtRepository->getNumberStudentOfFaculty($faculty);
        // return count($students);
    }
}
