<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221212134118 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE ie_results (id INT AUTO_INCREMENT NOT NULL, student_id INT NOT NULL, session_exam_id INT NOT NULL, course_id INT NOT NULL, response LONGTEXT DEFAULT NULL, file VARCHAR(255) DEFAULT NULL, note DOUBLE PRECISION DEFAULT NULL, begin_at DATETIME DEFAULT NULL, INDEX IDX_56455927CB944F1A (student_id), INDEX IDX_564559274555DCCB (session_exam_id), INDEX IDX_56455927591CC992 (course_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ie_results ADD CONSTRAINT FK_56455927CB944F1A FOREIGN KEY (student_id) REFERENCES ie_student_informations (id)');
        $this->addSql('ALTER TABLE ie_results ADD CONSTRAINT FK_564559274555DCCB FOREIGN KEY (session_exam_id) REFERENCES ie_exam_sessions (id)');
        $this->addSql('ALTER TABLE ie_results ADD CONSTRAINT FK_56455927591CC992 FOREIGN KEY (course_id) REFERENCES ie_course (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE ie_results');
    }
}
