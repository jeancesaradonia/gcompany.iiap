# VARIABLE

PWD:
	pwd


# LES COMMANDES

start-server:
	docker-compose up -d
	symfony serve -d
	symfony open:local
.PHONY: start-server


stop-server:
	docker-compose down --remove-orphans
	symfony server:stop
.PHONY: stop-server

migration:
	symfony console make:migration
	symfony console d:m:m
.PHONY: migration

php-7:
	docker run -it --rm -v "$(PWD)":/project/gci -w /project/gci gate:1 bash
.PHONY: php-7

export-db:
	$(eval CONTAINER_NAME := $(shell read -p "nom container? (tapez cette commande 'docker container ps' pour connaitre le nom du container MYSQL/MARIADB) : " CONTAINER_NAME && echo $${CONTAINER_NAME:-N}))
	docker exec $(CONTAINER_NAME) /usr/bin/mysqldump -u root --password=password iiap_education_db_dev > iiap_education_db_dev.sql
.PHONY: export-db