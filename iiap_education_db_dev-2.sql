-- MariaDB dump 10.19  Distrib 10.7.3-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: iiap_education_db_dev
-- ------------------------------------------------------
-- Server version	10.7.3-MariaDB-1:10.7.3+maria~focal

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8mb3_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctrine_migration_versions`
--

LOCK TABLES `doctrine_migration_versions` WRITE;
/*!40000 ALTER TABLE `doctrine_migration_versions` DISABLE KEYS */;
INSERT INTO `doctrine_migration_versions` VALUES
('DoctrineMigrations\\Version20220817145604','2022-08-17 16:56:15',2240),
('DoctrineMigrations\\Version20220822094330','2022-08-22 11:43:47',2250),
('DoctrineMigrations\\Version20220822110424','2022-08-22 13:04:31',2122),
('DoctrineMigrations\\Version20220824112343','2022-08-24 13:23:54',7689),
('DoctrineMigrations\\Version20220824134339','2022-08-24 15:43:52',859),
('DoctrineMigrations\\Version20220824134527','2022-08-24 15:45:30',637),
('DoctrineMigrations\\Version20220829115412','2022-08-29 13:54:17',447),
('DoctrineMigrations\\Version20220829152325','2022-08-29 17:23:31',1250),
('DoctrineMigrations\\Version20220905150446','2022-09-05 17:05:57',1181),
('DoctrineMigrations\\Version20220914123705','2022-09-14 14:39:15',7816),
('DoctrineMigrations\\Version20220914124506','2022-09-14 14:45:36',568),
('DoctrineMigrations\\Version20220914134340','2022-09-14 15:43:46',4014),
('DoctrineMigrations\\Version20220914135322','2022-09-14 15:54:48',6089),
('DoctrineMigrations\\Version20220914151344','2022-09-14 17:13:53',5189),
('DoctrineMigrations\\Version20220914154453','2022-09-14 17:44:58',7425),
('DoctrineMigrations\\Version20220914155942','2022-09-14 17:59:45',297),
('DoctrineMigrations\\Version20220916091855','2022-09-16 11:19:05',1976),
('DoctrineMigrations\\Version20220916155725','2022-09-16 17:57:33',2069),
('DoctrineMigrations\\Version20220921115420','2022-09-21 13:54:30',931),
('DoctrineMigrations\\Version20220921115507','2022-09-21 13:55:11',818),
('DoctrineMigrations\\Version20220921132508','2022-09-21 15:25:19',913),
('DoctrineMigrations\\Version20220921132753','2022-09-21 15:28:04',270),
('DoctrineMigrations\\Version20220921133308','2022-09-21 15:33:17',268),
('DoctrineMigrations\\Version20220921133426','2022-09-21 15:34:29',276),
('DoctrineMigrations\\Version20220923095853','2022-09-23 12:00:14',2977),
('DoctrineMigrations\\Version20220923100126','2022-09-23 12:01:34',298),
('DoctrineMigrations\\Version20220923103656','2022-09-23 12:37:14',2475),
('DoctrineMigrations\\Version20220923105036','2022-09-23 12:50:55',418),
('DoctrineMigrations\\Version20220930123512','2022-09-30 14:35:27',2616),
('DoctrineMigrations\\Version20220930130914','2022-09-30 15:09:22',753),
('DoctrineMigrations\\Version20220930131340','2022-09-30 15:13:45',1324),
('DoctrineMigrations\\Version20221005123132','2022-10-05 14:31:35',774),
('DoctrineMigrations\\Version20221010145343','2022-10-10 16:53:56',487),
('DoctrineMigrations\\Version20221017162200','2022-10-17 18:22:15',25885),
('DoctrineMigrations\\Version20221019094222','2022-10-19 11:42:29',772),
('DoctrineMigrations\\Version20221019100353','2022-10-19 12:03:57',285),
('DoctrineMigrations\\Version20221019153012','2022-10-19 17:30:43',8256),
('DoctrineMigrations\\Version20221026133051','2022-10-26 15:34:36',1279),
('DoctrineMigrations\\Version20221031095630','2022-10-31 10:56:33',2331),
('DoctrineMigrations\\Version20221031102715','2022-10-31 11:27:19',2825),
('DoctrineMigrations\\Version20221031112603','2022-10-31 12:26:05',846),
('DoctrineMigrations\\Version20221031114938','2022-10-31 12:49:42',2396);
/*!40000 ALTER TABLE `doctrine_migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ie_course`
--

DROP TABLE IF EXISTS `ie_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ie_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teaching_unit_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `credit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_FC03DACA2F82B390` (`teaching_unit_id`),
  CONSTRAINT `FK_FC03DACA2F82B390` FOREIGN KEY (`teaching_unit_id`) REFERENCES `ie_teaching_unit` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ie_course`
--

LOCK TABLES `ie_course` WRITE;
/*!40000 ALTER TABLE `ie_course` DISABLE KEYS */;
INSERT INTO `ie_course` VALUES
(4,4,'Money','Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at','screenshot-2022-10-24-11-31-00-63590d414a795605084935.png','3');
/*!40000 ALTER TABLE `ie_course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ie_course_content`
--

DROP TABLE IF EXISTS `ie_course_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ie_course_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `content_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_650819D2591CC992` (`course_id`),
  CONSTRAINT `FK_650819D2591CC992` FOREIGN KEY (`course_id`) REFERENCES `ie_course` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ie_course_content`
--

LOCK TABLES `ie_course_content` WRITE;
/*!40000 ALTER TABLE `ie_course_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `ie_course_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ie_degrees`
--

DROP TABLE IF EXISTS `ie_degrees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ie_degrees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ie_degrees`
--

LOCK TABLES `ie_degrees` WRITE;
/*!40000 ALTER TABLE `ie_degrees` DISABLE KEYS */;
INSERT INTO `ie_degrees` VALUES
(7,'LICENCE','Diplome de licence'),
(8,'MASTER','diplome de master');
/*!40000 ALTER TABLE `ie_degrees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ie_faculties`
--

DROP TABLE IF EXISTS `ie_faculties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ie_faculties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `acronym` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ie_faculties`
--

LOCK TABLES `ie_faculties` WRITE;
/*!40000 ALTER TABLE `ie_faculties` DISABLE KEYS */;
INSERT INTO `ie_faculties` VALUES
(9,'Droit, Administration Publique et Territoriale','DAPT','Droit, Administration Publique et Territoriale.Droit, Administration Publique et Territoriale'),
(10,'Gestion de projet et Développement Durable','GPDD','Gestion de projet et Développement Durable.Gestion de projet et Développement Durable'),
(11,'Marchés et Finances Publiques','MFP','Marchés et Finances Publiques.Marchés et Finances Publiques'),
(12,'Organisations Internationales, Humanitaires et Diplomatie','OIHD','Organisations Internationales, Humanitaires et Diplomatie.Organisations Internationales, Humanitaires et Diplomatie');
/*!40000 ALTER TABLE `ie_faculties` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ie_levels`
--

DROP TABLE IF EXISTS `ie_levels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ie_levels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `degree_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_75B7D16AB35C5756` (`degree_id`),
  CONSTRAINT `FK_75B7D16AB35C5756` FOREIGN KEY (`degree_id`) REFERENCES `ie_degrees` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ie_levels`
--

LOCK TABLES `ie_levels` WRITE;
/*!40000 ALTER TABLE `ie_levels` DISABLE KEYS */;
INSERT INTO `ie_levels` VALUES
(15,7,'Licence 1','Licence 1'),
(16,7,'Licence 2','Licence 2'),
(17,7,'Licence 3','Licence 3'),
(18,8,'Master 1','Master 1'),
(19,8,'Master 2','Master 2');
/*!40000 ALTER TABLE `ie_levels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ie_messages`
--

DROP TABLE IF EXISTS `ie_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ie_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_641B993DF624B39D` (`sender_id`),
  KEY `IDX_641B993DCD53EDB6` (`receiver_id`),
  CONSTRAINT `FK_641B993DCD53EDB6` FOREIGN KEY (`receiver_id`) REFERENCES `ie_users` (`id`),
  CONSTRAINT `FK_641B993DF624B39D` FOREIGN KEY (`sender_id`) REFERENCES `ie_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ie_messages`
--

LOCK TABLES `ie_messages` WRITE;
/*!40000 ALTER TABLE `ie_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `ie_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ie_page_ie_page`
--

DROP TABLE IF EXISTS `ie_page_ie_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ie_page_ie_page` (
  `ie_page_source` int(11) NOT NULL,
  `ie_page_target` int(11) NOT NULL,
  PRIMARY KEY (`ie_page_source`,`ie_page_target`),
  KEY `IDX_10D80616F5D89D1F` (`ie_page_source`),
  KEY `IDX_10D80616EC3DCD90` (`ie_page_target`),
  CONSTRAINT `FK_10D80616EC3DCD90` FOREIGN KEY (`ie_page_target`) REFERENCES `ie_pages` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_10D80616F5D89D1F` FOREIGN KEY (`ie_page_source`) REFERENCES `ie_pages` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ie_page_ie_page`
--

LOCK TABLES `ie_page_ie_page` WRITE;
/*!40000 ALTER TABLE `ie_page_ie_page` DISABLE KEYS */;
INSERT INTO `ie_page_ie_page` VALUES
(6,8),
(6,9),
(6,10),
(6,11);
/*!40000 ALTER TABLE `ie_page_ie_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ie_pages`
--

DROP TABLE IF EXISTS `ie_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ie_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `view` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `link_text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `menu_order` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ie_pages`
--

LOCK TABLES `ie_pages` WRITE;
/*!40000 ALTER TABLE `ie_pages` DISABLE KEYS */;
INSERT INTO `ie_pages` VALUES
(4,'Institut International des Affaires Publiques - Vers la méritocratie','welcome','Page d\'accueil; Institut International des Affaires Publiques - Vers la méritocratie','page/index.html',1,0,'Accueil',1),
(5,'Notre institution - Institut International des Affaires Publiques - Vers la méritocratie','notre-institution','Page : notre institution','page/notre-institution.html',1,0,'Notre institution',2),
(6,'Nos formations - Institut International des Affaires Publiques - Vers la méritocratie','nos-formations','Page : nos formations','page/nos-formations.html',1,0,'Nos formations',3),
(7,'Contact - Institut International des Affaires Publiques - Vers la méritocratie','contact','Page : Contact','page/contact.html',1,0,'Nous contacter',4),
(8,'Droit, Administration Publique et Territoriale (DAPT) : Institut International des Affaires Publiques - Vers la méritocratie','nos-formations-droit-administration-publique-et-territoriale','Page : Droit, Administration Publique et Territoriale (DAPT)','page/dapt.html',1,0,'DAPT',1),
(9,'Gestion de projet et Développement Durable (GPDD) : Institut International des Affaires Publiques - Vers la méritocratie','nos-formations-gestion-de-projet-et-developpement-durable','Page : gestion-de-projet-et-developpement-durable','page/gpdd.html',1,0,'GPDD',2),
(10,'Marchés et Finances Publiques : Institut International des Affaires Publiques - Vers la méritocratie','nos-formations-marches-et-finances-publiques','Page : marches-et-finances-publiques','page/mfp.html',1,0,'MFP',3),
(11,'Organisations Internationales, Humanitaires et Diplomatie : Institut International des Affaires Publiques - Vers la méritocratie','nos-formations-organisations-internationales-humanitaires-et-diplomatie','Page : organisations-internationales-humanitaires-et-diplomatie','page/oihd.html',1,0,'OIHD',4),
(12,'Recherche : Institut International des Affaires Publiques - Vers la méritocratie','recherche','Page : recherche','page/search.html',0,0,'<i class=\"fas fa-search\"></i>',6),
(13,'Inscription en ligne','inscription','Page : Page d\'inscription','registration/register.html',1,0,'S\'inscrire',5),
(17,'Authentification','connexion','Page de connexion','security/login-student.html',1,0,'Connexion',7);
/*!40000 ALTER TABLE `ie_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ie_payments`
--

DROP TABLE IF EXISTS `ie_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ie_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `reason` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `decision` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `observation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `validated_at` datetime DEFAULT NULL,
  `done_at` date NOT NULL,
  `reference_mobile_money` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_DACB1C99CB944F1A` (`student_id`),
  CONSTRAINT `FK_DACB1C99CB944F1A` FOREIGN KEY (`student_id`) REFERENCES `ie_student_informations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ie_payments`
--

LOCK TABLES `ie_payments` WRITE;
/*!40000 ALTER TABLE `ie_payments` DISABLE KEYS */;
INSERT INTO `ie_payments` VALUES
(27,30,'registration-fee','read','taken','190000','première paiment : ok','2022-10-26 12:27:32','2022-10-26','19818916556','mobile-money'),
(28,30,'monthly-fees','read','taken','400000','ok','2022-10-26 12:28:51','2022-10-26','015618918905','mobile-money');
/*!40000 ALTER TABLE `ie_payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ie_roles`
--

DROP TABLE IF EXISTS `ie_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ie_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ie_roles`
--

LOCK TABLES `ie_roles` WRITE;
/*!40000 ALTER TABLE `ie_roles` DISABLE KEYS */;
INSERT INTO `ie_roles` VALUES
(17,'ROLE_STUDENT','Étudiant'),
(18,'ROLE_ADMIN','Administrateur'),
(19,'ROLE_TEACHER','Proffesseur'),
(20,'ROLE_SCHOOL_MANAGER','Géstionnaire d\'écolage');
/*!40000 ALTER TABLE `ie_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ie_semesters`
--

DROP TABLE IF EXISTS `ie_semesters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ie_semesters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level_id` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_84F90AA75FB14BA7` (`level_id`),
  CONSTRAINT `FK_84F90AA75FB14BA7` FOREIGN KEY (`level_id`) REFERENCES `ie_levels` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ie_semesters`
--

LOCK TABLES `ie_semesters` WRITE;
/*!40000 ALTER TABLE `ie_semesters` DISABLE KEYS */;
INSERT INTO `ie_semesters` VALUES
(1,15,1,1),
(2,15,2,1),
(3,16,3,2),
(4,16,4,2),
(5,17,5,3),
(6,17,6,3),
(7,18,7,4),
(8,18,8,4),
(9,19,9,5),
(10,19,10,5);
/*!40000 ALTER TABLE `ie_semesters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ie_student_informations`
--

DROP TABLE IF EXISTS `ie_student_informations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ie_student_informations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level_id` int(11) NOT NULL,
  `faculty_id` int(11) NOT NULL,
  `matricule_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employer_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fm_names` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fm_jobs` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fm_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fm_phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fm_adress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `t_names` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `t_jobs` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `t_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `t_phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `t_adress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `g_names` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `g_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `g_phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `formation_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cin_photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `curriculum_vitae` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note_or_copy_of_the_certified_diploma` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `residence_certificate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_certificate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `motivation_letter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sexe` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `native_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `semester_id` int(11) DEFAULT NULL,
  `vague_id` int(11) DEFAULT NULL,
  `registration_fee` int(11) DEFAULT NULL,
  `monthly_fees` int(11) DEFAULT NULL,
  `examination_fees` int(11) DEFAULT NULL,
  `defense_fee` int(11) DEFAULT NULL,
  `certificate_fee` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_ED90405A5FB14BA7` (`level_id`),
  KEY `IDX_ED90405A680CAB68` (`faculty_id`),
  KEY `IDX_ED90405A4A798B6F` (`semester_id`),
  KEY `IDX_ED90405A93E74B61` (`vague_id`),
  CONSTRAINT `FK_ED90405A4A798B6F` FOREIGN KEY (`semester_id`) REFERENCES `ie_semesters` (`id`),
  CONSTRAINT `FK_ED90405A5FB14BA7` FOREIGN KEY (`level_id`) REFERENCES `ie_levels` (`id`),
  CONSTRAINT `FK_ED90405A680CAB68` FOREIGN KEY (`faculty_id`) REFERENCES `ie_faculties` (`id`),
  CONSTRAINT `FK_ED90405A93E74B61` FOREIGN KEY (`vague_id`) REFERENCES `ie_vagues` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ie_student_informations`
--

LOCK TABLES `ie_student_informations` WRITE;
/*!40000 ALTER TABLE `ie_student_informations` DISABLE KEYS */;
INSERT INTO `ie_student_informations` VALUES
(30,15,9,'DAPT-V1/001/FLI','- ','Developpeur et designer','worker',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'- ','-','- ','online','Screenshot-2022-10-17-14-32-22-63590a98791a7.png','Screenshot-2022-10-17-14-32-22-63590a98b7834.png','Screenshot-2022-10-24-12-29-47-63590a98b7ab5.png','Screenshot-2022-10-24-12-37-03-63590a98b7d2c.png','Screenshot-2022-08-12-22-10-34-63590a98b8039.png','Screenshot-2022-10-24-12-44-39-63590a98b83b2.png','male','MG',1,1,1,2,0,0,0);
/*!40000 ALTER TABLE `ie_student_informations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ie_taught`
--

DROP TABLE IF EXISTS `ie_taught`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ie_taught` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `faculty_id` int(11) NOT NULL,
  `semester_id` int(11) NOT NULL,
  `teacher_information_id` int(11) NOT NULL,
  `course_id` int(11) DEFAULT NULL,
  `month` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_848BDC7B680CAB68` (`faculty_id`),
  KEY `IDX_848BDC7B4A798B6F` (`semester_id`),
  KEY `IDX_848BDC7B7950C5AA` (`teacher_information_id`),
  KEY `IDX_848BDC7B591CC992` (`course_id`),
  CONSTRAINT `FK_848BDC7B4A798B6F` FOREIGN KEY (`semester_id`) REFERENCES `ie_semesters` (`id`),
  CONSTRAINT `FK_848BDC7B591CC992` FOREIGN KEY (`course_id`) REFERENCES `ie_course` (`id`),
  CONSTRAINT `FK_848BDC7B680CAB68` FOREIGN KEY (`faculty_id`) REFERENCES `ie_faculties` (`id`),
  CONSTRAINT `FK_848BDC7B7950C5AA` FOREIGN KEY (`teacher_information_id`) REFERENCES `ie_teacher_informations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ie_taught`
--

LOCK TABLES `ie_taught` WRITE;
/*!40000 ALTER TABLE `ie_taught` DISABLE KEYS */;
INSERT INTO `ie_taught` VALUES
(3,9,1,1,4,1);
/*!40000 ALTER TABLE `ie_taught` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ie_teacher_informations`
--

DROP TABLE IF EXISTS `ie_teacher_informations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ie_teacher_informations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `salary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E29105DFA76ED395` (`user_id`),
  CONSTRAINT `FK_E29105DFA76ED395` FOREIGN KEY (`user_id`) REFERENCES `ie_users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ie_teacher_informations`
--

LOCK TABLES `ie_teacher_informations` WRITE;
/*!40000 ALTER TABLE `ie_teacher_informations` DISABLE KEYS */;
INSERT INTO `ie_teacher_informations` VALUES
(1,35,'1000000');
/*!40000 ALTER TABLE `ie_teacher_informations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ie_teaching_unit`
--

DROP TABLE IF EXISTS `ie_teaching_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ie_teaching_unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ie_teaching_unit`
--

LOCK TABLES `ie_teaching_unit` WRITE;
/*!40000 ALTER TABLE `ie_teaching_unit` DISABLE KEYS */;
INSERT INTO `ie_teaching_unit` VALUES
(4,'Gestion général');
/*!40000 ALTER TABLE `ie_teaching_unit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ie_timetables`
--

DROP TABLE IF EXISTS `ie_timetables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ie_timetables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `faculty_id` int(11) DEFAULT NULL,
  `vague_id` int(11) DEFAULT NULL,
  `semester_id` int(11) DEFAULT NULL,
  `start_at` datetime NOT NULL,
  `end_at` datetime NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_7522C379680CAB68` (`faculty_id`),
  KEY `IDX_7522C37993E74B61` (`vague_id`),
  KEY `IDX_7522C3794A798B6F` (`semester_id`),
  KEY `IDX_7522C3795FB14BA7` (`level_id`),
  CONSTRAINT `FK_7522C3794A798B6F` FOREIGN KEY (`semester_id`) REFERENCES `ie_semesters` (`id`),
  CONSTRAINT `FK_7522C3795FB14BA7` FOREIGN KEY (`level_id`) REFERENCES `ie_levels` (`id`),
  CONSTRAINT `FK_7522C379680CAB68` FOREIGN KEY (`faculty_id`) REFERENCES `ie_faculties` (`id`),
  CONSTRAINT `FK_7522C37993E74B61` FOREIGN KEY (`vague_id`) REFERENCES `ie_vagues` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ie_timetables`
--

LOCK TABLES `ie_timetables` WRITE;
/*!40000 ALTER TABLE `ie_timetables` DISABLE KEYS */;
/*!40000 ALTER TABLE `ie_timetables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ie_user_ie_role`
--

DROP TABLE IF EXISTS `ie_user_ie_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ie_user_ie_role` (
  `ie_user_id` int(11) NOT NULL,
  `ie_role_id` int(11) NOT NULL,
  PRIMARY KEY (`ie_user_id`,`ie_role_id`),
  KEY `IDX_F1D6CFC36E886EA6` (`ie_user_id`),
  KEY `IDX_F1D6CFC31FE59F9F` (`ie_role_id`),
  CONSTRAINT `FK_F1D6CFC31FE59F9F` FOREIGN KEY (`ie_role_id`) REFERENCES `ie_roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_F1D6CFC36E886EA6` FOREIGN KEY (`ie_user_id`) REFERENCES `ie_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ie_user_ie_role`
--

LOCK TABLES `ie_user_ie_role` WRITE;
/*!40000 ALTER TABLE `ie_user_ie_role` DISABLE KEYS */;
INSERT INTO `ie_user_ie_role` VALUES
(16,18),
(32,20),
(35,19),
(42,17);
/*!40000 ALTER TABLE `ie_user_ie_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ie_users`
--

DROP TABLE IF EXISTS `ie_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ie_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `student_information_id` int(11) DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_date` datetime DEFAULT NULL,
  `birth_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirm_at` datetime DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `is_verified` tinyint(1) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_5C71EF6DE7927C74` (`email`),
  KEY `IDX_5C71EF6D391213BA` (`student_information_id`),
  CONSTRAINT `FK_5C71EF6D391213BA` FOREIGN KEY (`student_information_id`) REFERENCES `ie_student_informations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ie_users`
--

LOCK TABLES `ie_users` WRITE;
/*!40000 ALTER TABLE `ie_users` DISABLE KEYS */;
INSERT INTO `ie_users` VALUES
(16,'adminiiap@gmail.com','$2y$13$34paURSFNwhnZ81oFQGk2.UDTvNy2Yb6CirVKlT/A/0lUIEjNuPJa','2022-09-21 15:07:26','2022-10-26 12:33:21',NULL,'Admin','IIAP',NULL,NULL,NULL,NULL,'gc-1-63569adaad8fe779760596.png',NULL,0,1,1),
(32,'geiiap@gmail.com','$2y$13$aIy2Z9gqpHsL6frYYC83t.YtrhDNM2nBjCCs08f/AtMmexbvCsc0W','2022-09-21 15:07:26','2022-10-26 12:29:03',NULL,'Gestionnaire','écolage',NULL,NULL,NULL,NULL,'gc-1-63569adaad8fe779760596.png',NULL,0,1,0),
(35,'teacheriiap@gmail.com','$2y$13$1I53gCCA9UrdFnwUQnWH9.cq8GVFON4/nEoP1ssm27JUpt2aXDWvK','2022-10-21 13:01:15','2022-10-21 13:01:15',NULL,'Teacher','IIAP',NULL,NULL,NULL,NULL,'gc-1-63569adaad8fe779760596.png',NULL,0,1,NULL),
(42,'rabarisonheriniainamg@gmail.com','$2y$13$ghDLavGjGGJSIOKXLPB31eyaClBuIBT2MLrh9jcutPCF5e2QyBbt6','2022-10-26 12:23:20','2022-10-26 12:33:05',30,'Heriniaina','RABARISON','2000-01-01 00:00:00','Morondava','Lot BM 251 Morarano Ampitatafika','0343010307','jonathan-borba-zfpoelmdc-m-unsplash-63590a98b904c540103447.jpg','2022-10-26 12:24:19',0,1,0);
/*!40000 ALTER TABLE `ie_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ie_vagues`
--

DROP TABLE IF EXISTS `ie_vagues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ie_vagues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `v_number` int(11) NOT NULL,
  `v_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ie_vagues`
--

LOCK TABLES `ie_vagues` WRITE;
/*!40000 ALTER TABLE `ie_vagues` DISABLE KEYS */;
INSERT INTO `ie_vagues` VALUES
(1,1,'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum ipsum molestias nisi numquam molestiae, dolor atque vero dolore omnis ratione nulla?','2022-10-21 16:44:00'),
(2,2,'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum ipsum molestias nisi numquam molestiae, dolor atque vero dolore omnis ratione nulla?',NULL),
(3,3,'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum ipsum molestias nisi numquam molestiae, dolor atque vero dolore omnis ratione nulla?',NULL),
(4,4,'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum ipsum molestias nisi numquam molestiae, dolor atque vero dolore omnis ratione nulla?',NULL),
(5,5,'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum ipsum molestias nisi numquam molestiae, dolor atque vero dolore omnis ratione nulla?',NULL),
(6,6,'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum ipsum molestias nisi numquam molestiae, dolor atque vero dolore omnis ratione nulla?','2022-10-26 15:44:11'),
(7,7,NULL,NULL),
(8,7,NULL,NULL),
(9,9,NULL,NULL),
(10,10,NULL,NULL);
/*!40000 ALTER TABLE `ie_vagues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reset_password_request`
--

DROP TABLE IF EXISTS `reset_password_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reset_password_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `selector` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hashed_token` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `requested_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `expires_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`id`),
  KEY `IDX_7CE748AA76ED395` (`user_id`),
  CONSTRAINT `FK_7CE748AA76ED395` FOREIGN KEY (`user_id`) REFERENCES `ie_users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reset_password_request`
--

LOCK TABLES `reset_password_request` WRITE;
/*!40000 ALTER TABLE `reset_password_request` DISABLE KEYS */;
/*!40000 ALTER TABLE `reset_password_request` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-10-31 11:50:23
