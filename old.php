<?php

namespace App\Entity;

use App\Entity\IeRole;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\Timestampable;
use App\Repository\IeUserRepository;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\Collections\ArrayCollection;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;

/**
 * @ORM\Entity(repositoryClass=IeUserRepository::class)
 * @ORM\Table(name="ie_users")
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 * @ORM\HasLifecycleCallbacks
 * @Vich\Uploadable
 */
class IeUsededdr implements UserInterface, PasswordAuthenticatedUserInterface
{
    use Timestampable;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\NotBlank(message="Cette valeur ne doit pas être vide.")
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Cette valeur ne doit pas être vide.")
     * @Assert\Length(min=3, minMessage="Cette valeur est trop courte. Il doit avoir 3 caractères ou plus.")
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Cette valeur ne doit pas être vide.")
     * @Assert\Length(min=3, minMessage="Cette valeur est trop courte. Il doit avoir 3 caractères ou plus.")
     */
    private $lastName;

    /**
     * @ORM\Column(type="datetime", length=255)
     * @Assert\NotBlank(message="Cette valeur ne doit pas être vide.")
     */
    private $birthDate;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Cette valeur ne doit pas être vide.")
     */
    private $birthPlace;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * 
     */
    private $avatar;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isDeleted;


    /**
     * @ORM\ManyToMany(targetEntity=IeRole::class, inversedBy="ieUsers")
     */
    private $ieRoles;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Cette valeur ne doit pas être vide.")
     */
    private $address;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isVerified = false;

    /**
     * @Vich\UploadableField(mapping="avatars", fileNameProperty="avatar")
     * @Assert\NotBlank(message="Vous devez avoir une image.")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $confirmAt;

    public function __construct()
    {
        $this->ieRoles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getBirthDate(): ?\DateTime
    {
        return $this->birthDate;
    }

    public function setBirthDate(\DateTime $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getBirthPlace(): ?string
    {
        return $this->birthPlace;
    }

    public function setBirthPlace(string $birthPlace): self
    {
        $this->birthPlace = $birthPlace;

        return $this;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setAvatar(?string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function isIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): self
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }


    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $userRoles = $this->getIeRoles();
        $roles = [];
        foreach ($userRoles as $userRole) {
            $roles[] = $userRole->getName();
        }
        return array_unique($roles);
    }

    /**
     * @return Collection<int, IeRole>
     */
    public function getIeRoles(): Collection
    {
        return $this->ieRoles;
    }

    public function addIeRole(IeRole $ieRole): self
    {
        if (!$this->ieRoles->contains($ieRole)) {
            $this->ieRoles[] = $ieRole;
        }

        return $this;
    }

    public function removeIeRole(IeRole $ieRole): self
    {
        $this->ieRoles->removeElement($ieRole);

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function getFullName()
    {
        return $this->firstName . ' ' . $this->lastName;
    }

    public function getFullBirth()
    {

        return $this->birthDate->format('d-m-Y') . ' à ' . $this->birthPlace;
    }

    public function getConfirmAt(): ?\DateTimeInterface
    {
        return $this->confirmAt;
    }

    public function setConfirmAt(?\DateTimeInterface $confirmAt): self
    {
        $this->confirmAt = $confirmAt;

        return $this;
    }
}
